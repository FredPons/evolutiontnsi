import pygame
import socket
import time
import threading
import queue
import sys
import os
from menu.chargerMenu import chargerMenu
from _Initialisation_.initialiser import *
from _Initialisation_.chargerImages import *
from _Initialisation_.determinerJoueur import *
from _Initialisation_.Creer.creerCases import *
from _Initialisation_.Creer.MesClasses.pieces import *
from Fonctions.Afficher.afficherall import *
from Fonctions.Deplacer.Deplacement import *
from Fonctions.EntreDeux.apresCoup import *



class Client:
    
    def __init__(self):
        self.pseudo = None
        self.elo = None
        self.socket = None

    def connect_to_server(self):
        """Se connecte au serveur
        """

        while True:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                server_address = ("89.84.157.56", 5555)
                self.socket.connect(server_address)

                return

            except (socket.error, ConnectionRefusedError):
                # Gérer les erreurs de connexion
                print("Échec de la connexion au serveur. Tentative de reconnexion...")
                time.sleep(5)  # Attendre quelques secondes avant de réessayer

    def run(self,menus,fenetre):
        """File d'attente

        Args:
            menus (list): liste de tout les menus
            fenetre (object): fenetre pygame

        Returns:
            menuFin (int) : menu affichant la fin de partie
        """
        qData = queue.Queue()
        thread = threading.Thread(target=threadReceive, args=(self.socket,qData))
        thread.start()
        while True:
            try:
                self.socket.sendall("start".encode())
                data = None
                frame_index = 0
                frame_rate = 60
                clock = pygame.time.Clock()
                while not data:
                    for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                            pygame.quit()
                    fenetre.fill((0,0,0))
                    current_frame = menus[-2].element[0][frame_index]
                    fenetre.blit(current_frame, (0, 0))
                    frame_index = (frame_index + 1) % len(menus[-2].element[0])
                    clock.tick(frame_rate)
                    pygame.display.flip()

                    try:
                        data = qData.get_nowait()
                    except: 
                        pass
                    if not data:
                        pass
                joueur = determinerJoueur(data)
                try:
                    data = qData.get()
                except: 
                    pass
                if not data:
                    pass
                if data == "play":
                    canPlay=True
                    wait = False
                    menuFin = jeu(self.socket,joueur,canPlay,wait,qData)
                    print(menuFin)
                    return menuFin
                elif data == "wait": 
                    wait = True
                    canPlay=False
                    menuFin = jeu(self.socket,joueur,canPlay,wait,qData)
                    print(menuFin)
                    return menuFin
            except (socket.error, ConnectionResetError):
                # Gérer les erreurs de connexion
                print("Connexion perdue. Tentative de reconnexion...")
                self.socket.close()
                self.connect_to_server()




def socket_receive_all_data(socket, data_len):
    """Récupère les données en fonction d'un nombre d'octets définit par data_len

    Args:
        socket (object): socket du joueur
        data_len (int): nombre d'octets

    Returns:
        total_data : data récuperer via le socket
    """
    MAX_DATA_SIZE = 1024
    current_data_len = 0
    total_data = None
    while current_data_len < data_len:
        chunk_len = data_len - current_data_len
        if chunk_len > MAX_DATA_SIZE:
            chunk_len = MAX_DATA_SIZE
        data = socket.recv(chunk_len)
        if not data:
            return None
        if not total_data:
            total_data = data
        else:
            total_data += data
        current_data_len += len(data)
    print(total_data)
    return total_data  


def threadReceive(client_socket,qData):
    """
        Recois les données du serveur et les transformes en liste
    Args:
        client_socket (_type_): _description_
        qData : permet de transferer des données entre les threads
    Returns:
        list : pieces,caseNew, Echec, Grab, banc
    """
    while True:
        header_data = socket_receive_all_data(client_socket, 13)
        longeur_data = int(header_data.decode())
        data = socket_receive_all_data(client_socket, longeur_data)
        print(data)
        if not data:
            print("Connexion au serveur perdue.")
            return
        data = data.decode()
        if data == "p1" or data=="p2":
            qData.put_nowait(data)
        elif data == "play" or data =="wait":
            qData.put_nowait(data)
        elif data == "END,1":
            data= False, 1
            qData.put_nowait(data)
        elif data == "END,2":
            data = False, 2
            qData.put_nowait(data)
        elif data == "END,3":
            data =  False, 3
            qData.put_nowait(data)
        elif data == "END,4":
            data =  False, 4
            qData.put_nowait(data)
        elif data == "END,5":
            data =  False, 5
            qData.put_nowait(data)
        elif str(data).startswith("DISC"):
            while True:
                data = client_socket.recv(8196)
                if not data:
                    return
                elif data == "END,6":
                    data = False,6
                    qData.put_nowait(data)
                data = data.split(",")
                fenetre = pygame.display.set_mode(50,50)
                fenetre 
                pygame.display.set_caption(f"{data[1]}")
                fenetre.blit(50,50)
                pygame.display.flip()
        else:

            data = data.split("/")
            pieces = data[0]
            cases = data[1]
            Echec = data[2]
            Grab = data[3]
            banc = data[4]
            pieces = pieces.split(";")
            pieces.pop(-1)
            for i in range (len(pieces)):
                pieces[i]= pieces[i].split(",")
            for i in range (len(pieces)) :
                for k in range (5) :
                    pieces[i][k] = int(pieces[i][k])
                    
            
            elements = cases.split(';')
            elements.pop(-1)
            caseNew = []
            for i in range(0, 100, 10):
                ligne = elements[i:i+10]
                caseNew.append([valeur.split(',') for valeur in ligne])
            for i in caseNew:
                for case in i :
                    if case[0] == "True":
                        case[0] = True
                    elif case[0] == "False":
                        case[0] = False
                    case[1] = int(case[1])
            Echec = Echec.split(",")
            Grab = Grab.split(";")
            Grab.pop(-1)
            for i in range(len(Grab)):
                Grab[i] = Grab[i].split("§")
                if Grab[i][1] == "None":
                    Grab[i][1] = None
                else: 
                    for k in range(1,4) :
                        Grab[i][k] = int(Grab[i][k])
            if banc == "":
                banc =[]
            else:
                banc = banc.split(";")
                banc.pop(-1)
                for i in range (len(banc)):
                    banc[i]= banc[i].split(",")
                for i in range (len(banc)) :
                    for k in range (5) :
                        banc[i][k] = int(banc[i][k])
            qData.put_nowait((pieces,caseNew,Echec,Grab,banc))

def packData(pieces, cases, echec, grab, banc):
    """Encore tout les données

    Args:
        pieces (list): liste de pièces
        cases (list): liste de cases
        echec (list): liste contenant si il y a échec pour chaque joueur
        grab (list): liste contenant si il y a grab pour chaque joueur
        banc (list): liste conteant les pièces dans le banc

    Returns:
        dataPack : données encodées
    """
    packListPiece = b""
    packListCase = b""
    packListGrab = b""
    packListPieceBanc = b""
    dataPack = b""
    for niveau in pieces:
        for sousniveau in niveau:
            for piece in sousniveau:
                pieceValue = [str(piece.position[0]),str(piece.position[1]) , str(piece.niveau), str(piece.sousniveau), str(piece.couleur)]
                strPieceValue = ",".join(pieceValue)
                packPiece = strPieceValue.encode()
                packListPiece += packPiece + b";"
    for ligne in cases:
        for case in ligne:
            caseValue = [str(case[0]), str(case[1])]
            strCaseValue =",".join(caseValue)

            packCase = strCaseValue.encode()
            packListCase+=packCase + b";"
    echec = [str(echec[0][0]), str(echec[0][1]),str(echec[1][0]), str(echec[1][1])]
    strEchec = ",".join(echec)
    packEchec = strEchec.encode()
    for element in grab:
        if element[1] == None:
            element=[str(element[0]),str(element[1])]
            strGrab = "§".join(element)
            packGrab = strGrab.encode()
            packListGrab += packGrab + b";"
        else:
            pieceGrabValue = [str(element[1].position[0]),str(element[1].position[1]) , str(element[1].couleur)]
            strPieceGrabValue = "§".join(pieceGrabValue)
            strGrab = str(element[0]) + "§" + strPieceGrabValue
            packGrab = strGrab.encode()
            packListGrab += packGrab + b";"
    for piece in banc.piecesshop:
        pieceBancValue = [str(piece.position[0]),str(piece.position[1]) , str(piece.niveau), str(piece.sousniveau), str(piece.couleur)]
        strPieceBancValue = ",".join(pieceBancValue)
        packPieceBanc = strPieceBancValue.encode()
        packListPieceBanc += packPieceBanc + b";"
    
    dataPack = packListPiece +b"/" +packListCase + b"/" + packEchec + b"/" + packListGrab + b"/" + packListPieceBanc

    return dataPack

def sendData(client_socket, dataPack):
    """Envoie les données au serveur

    Args:
        client_socket (object): socket du joueur
        dataPack (binary): données encodées
    """
    data_len = len(dataPack)
    header = str(data_len).zfill(13)
    print("header:", header)
    header = header.encode()
    client_socket.sendall(header)
    if data_len > 0:
        client_socket.sendall(dataPack)

def relistepiece(piecesrecu):
    """reliste toute les pièces recues 

    Args:
        piecesrecu (list): list des pièces

    Returns:
        pieces : liste des pièces
    """
    boitePieces = BoitePieces()
    pieces = boitePieces.pieces
    for piece in piecesrecu:
        position=(piece[0],piece[1])
        couleur=piece[4]
        niveau=piece[2]
        sousniveau=piece[3]
        if niveau==0:
            gardien = Gardien(couleur,position)
            boitePieces.add_piece(gardien,pieces)
        elif niveau==1:
            soldat = Soldat(couleur,position)
            boitePieces.add_piece(soldat,pieces)
        elif niveau==2:
            if sousniveau==0:
                bastion = Bastion(couleur,position)
                boitePieces.add_piece(bastion,pieces)
            if sousniveau==1:
                jumper = Jumper(couleur,position)
                boitePieces.add_piece(jumper,pieces)
        elif niveau==3:
            if sousniveau==0:
                donjon = Donjon(couleur,position)
                boitePieces.add_piece(donjon,pieces)
            if sousniveau==1:
                chapelain = Chapelain(couleur,position)
                boitePieces.add_piece(chapelain,pieces)
        elif niveau==4:
            empereur = Empereur(couleur,position)
            boitePieces.add_piece(empereur,pieces)
        pieces = boitePieces.pieces
    return boitePieces,pieces

def relistepieceGrab(grabrecu):
    for i in range(len(grabrecu)):
        if grabrecu[i][0] == "True":
            position=(grabrecu[i][1],grabrecu[i][2])
            couleur=grabrecu[i][3]
            soldat = Soldat(couleur,position)
            grabrecu[i] = [True, soldat]
        elif grabrecu[i][0] == "False":
           grabrecu[i] =  [False, None]
        
    return grabrecu

def relisteechecGrab(echecrecu):
    check=[[],[]]
    if echecrecu[0]=="False":
        check[0]=[False,0]
    elif echecrecu[0]=="True":
        check[0]=[True,1]
    if echecrecu[2]=="False":
        check[1]=[False,0]
    elif echecrecu[2]=="True":
        check[1]=[True,2]
        
    return check

def relistepieceBanc(bancRecu):
    banc = Banc()
    piecesshop = banc.piecesshop
    
    for piece in bancRecu:
        position=(piece[0],piece[1])
        couleur=piece[4]
        niveau=piece[2]
        sousniveau=piece[3]
        if niveau==1:
            soldat = Soldat(couleur,position)
            banc.add_piece(soldat,piecesshop)
        elif niveau==2:
            if sousniveau==0:
                bastion = Bastion(couleur,position)
                banc.add_piece(bastion,piecesshop)
            if sousniveau==1:
                jumper = Jumper(couleur,position)
                banc.add_piece(jumper,piecesshop)
        elif niveau==3:
            if sousniveau==0:
                donjon = Donjon(couleur,position)
                banc.add_piece(donjon,piecesshop)
            if sousniveau==1:
                chapelain = Chapelain(couleur,position)
                banc.add_piece(chapelain,piecesshop)
        elif niveau==4:
            empereur = Empereur(couleur,position)
            banc.add_piece(empereur,piecesshop)
        piecesshop = banc.piecesshop
    return banc,piecesshop

def jeu(client_socket,joueur,canPlay,wait,qData):
    """Boucle princpale du jeu 

    Args:
        client_socket (object): socket du joueur
        joueur (int): numéro du joueur
        canPlay (bool): peut jouer où non
        wait (bool): attendre où non
        qData (object): queue pour transferer les données

    Returns:
        menuFin : menu pour la fin de partie
    """
    pygame.init()
    pygame.mixer.init()
    fenetrejeu = pygame.display.set_mode((1920, 1080))
    clicSon=pygame.mixer.Sound('clicSon.mp3')
    clicSon.set_volume(1)
    images_jeu=chargerImages()
    boitePieces,cases,pieces=initialiser()
    banc=Banc()
    Echec=[[False,0],[False,0]]
    Grab=[[False,None],[False,None]]
    choixremettre=(False,(0,0))
    piecebase=None
    caseCanMove=[]
    casesRefresh=[]
    caseGrab=[]
    clic=False
    run = True
    check=0
    while run:
        fenetrejeu.fill((44, 44, 44))
        afficherall(afficherplateau,afficherpieces,afficherpts,fenetrejeu,images_jeu,pieces,joueur,caseCanMove,clic,piecebase,
                    Echec,caseGrab,Grab,banc,choixremettre,casesRefresh)
        pygame.display.update()
        while canPlay == True:
            for i in range(0,1):
                if Grab[i][1]!=None and Grab[i][1].niveau!=1:
                    Grab[i][1]=None
            pieces=boitePieces.pieces
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    sys.exit()
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if pygame.mouse.get_pressed()[0]:
                        x,y = pygame.mouse.get_pos()
                        if clic==False:
                            caseCanMove,possibiliteDeBouger,caseclic,Canfusion,piecebase,caseGrab,pieceGrab,pieces,choixremettre=bougerall(x,y,
                                joueur,cases,boitePieces,caseGrab,Grab,banc,choixremettre)
                            clic,Echec=changerclics(Echec,clic,possibiliteDeBouger,caseCanMove,x,y,joueur,pieces,cases,caseclic,boitePieces,
                                                Canfusion,piecebase,images_jeu,fenetrejeu,caseGrab,pieceGrab,Grab,banc,choixremettre,canPlay)
                            okrefresh = False
                        else:
                            if choixremettre[0]==False:
                                clic,pieces,cases,Echec,caseGrab,Grab,canPlay=changerclics(Echec,clic,possibiliteDeBouger,caseCanMove,x,y,
                                                            joueur,pieces,cases,caseclic,boitePieces,Canfusion,
                                                        piecebase,images_jeu,fenetrejeu,caseGrab,pieceGrab,Grab,banc,choixremettre,canPlay)
                            elif choixremettre[0]==True:
                                if okrefresh==False:
                                    banc,refresh,casesRefresh=changerclics(Echec,clic,possibiliteDeBouger,caseCanMove,x,y,
                                                        joueur,pieces,cases,caseclic,boitePieces,Canfusion,
                                                    piecebase,images_jeu,fenetrejeu,caseGrab,pieceGrab,Grab,banc,choixremettre,canPlay)
                                if refresh==True:
                                    if okrefresh==True:
                                        banc,clic,boitePieces.pieces,cases,canPlay=changerclics2(banc,x,y,
                                        joueur,choixremettre,casesRefresh,boitePieces,clic,cases,canPlay)
                                        okrefresh=False
                                        choixremettre=(False,(0,0))
                                        casesRefresh=[]
                                    else:
                                        okrefresh = True
                                else:
                                    choixremettre=(False,(0,0))
            if canPlay == False:
                Grab=[(False,None),(False,None)]
                dataPack = packData(pieces,cases,Echec,Grab, banc)
                sendData(client_socket,dataPack)
                wait = True
                pygame.mixer.Sound.play(clicSon)
            fenetrejeu.fill((0, 0, 0))
            afficherall(afficherplateau,afficherpieces,afficherpts,fenetrejeu,images_jeu,pieces,joueur,caseCanMove,clic,piecebase,
                    Echec,caseGrab,Grab,banc,choixremettre,casesRefresh)
            pygame.display.update()
        while wait == True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                    sys.exit()
            data = []
            try:
                data = qData.get_nowait()
            except: 
                pass
            if not data:
                pass
            else:
                pygame.mixer.Sound.play(clicSon)
                if data[0] == False:
                    if data[1] == 1 or data[1]==2:
                        print('Vous avez Gagné par Echec et Mat')
                        run=False
                        canPlay=False
                        wait = False
                        return 0
                    elif data[1] == 3 :
                        print('Vous avez fait égalité par double manque de pieces majeures')
                        run=False
                        canPlay=False
                        wait = False
                        return 4 
                    elif data[1] == 4 :
                        print('Vous avez gagné par manque de matériel')
                        run=False
                        canPlay=False
                        wait = False
                        return 2 
                    elif data[1] == 5 :
                        print('Vous avez fais égalité par échec perpétuel(15)')
                        run=False
                        canPlay=False
                        wait = False
                        return 5 
                    elif data[1] == 6:
                        print("Vous avez gagné par déconnexion de l adversaire")
                        run=False
                        canPlay=False
                        wait = False
                        return 6 
                else:
                    pieces = data[0]
                    boitePieces, pieces = relistepiece(pieces)
                    cases = data[1]
                    Echec = data[2]
                    Echec =relisteechecGrab(Echec)
                    Grab = data[3]
                    Grab = relistepieceGrab(Grab)
                    banc = data[4]
                    banc, banc.piecesshop = relistepieceBanc(banc)
                    wait = False
                    canPlay = True
            fenetrejeu.fill((0, 0, 0))
            afficherall(afficherplateau,afficherpieces,afficherpts,fenetrejeu,images_jeu,pieces,joueur,caseCanMove,clic,piecebase,
                    Echec,caseGrab,Grab,banc,choixremettre,casesRefresh)
            pygame.display.update()
        run,wait,canPlay,choixremettre,piecebase,caseCanMove,casesRefresh,caseGrab,clic,check,menuFin=apresCoup(Echec,joueur,
            cases,boitePieces,client_socket,banc,pieces,run,wait,canPlay,choixremettre,piecebase,caseCanMove,casesRefresh,caseGrab,clic,check)      
        fenetrejeu.fill((0, 0, 0))
        afficherall(afficherplateau,afficherpieces,afficherpts,fenetrejeu,images_jeu,pieces,joueur,caseCanMove,clic,piecebase,
                    Echec,caseGrab,Grab,banc,choixremettre,casesRefresh)
        pygame.display.update()

    return menuFin


def main():
    """boucle principale 
    Gère l'affichage des menus
    """
    pygame.init()
    fenetre = pygame.display.set_mode((1920, 1080))
    clock = pygame.time.Clock() 
    police = pygame.font.SysFont(None, 45)
    menus = chargerMenu(fenetre)
    i= 6
    k=0
    client = Client()
    client.connect_to_server()
    while True:
        clic = [False]*len(menus[i].elementClic)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN or i in [7,8,9,10,-1]:
                if pygame.mouse.get_pressed()[0] or i in [7,8,9,10,-1]:
                    pos = pygame.mouse.get_pos()
                    if i not in [7,8,9,10,-1]:
                        clic = menus[i].clic(pos[0],pos[1])
                    if i == 0:
                        if clic[0] == True:
                            i=0
                        elif clic[1] == True:
                            i=1
                        elif clic[2] == True:
                            i=2
                        elif clic[3] == True:
                            i=3
                        elif clic[4] == True:
                            i=4
                        elif clic[5] == True:
                            i=5
                        elif clic[6] == True:
                            pass
                        elif clic[7] == True:
                            k = client.run(menus,fenetre)
                            i= -1
                        elif clic[8] == True:
                            i = 4
                    elif i == 1:
                        if clic[0] == True:
                            i=0
                        elif clic[1] == True:
                            i=1
                        elif clic[2] == True:
                            i=2
                        elif clic[3] == True:
                            i=3
                        elif clic[4] == True:
                            i=4
                        elif clic[5] == True:
                            i=5
                        elif clic[6] == True:
                            pass
                        elif clic[7] == True:
                            k = client.run(menus,fenetre)
                            i= -1
                    elif i == 2:
                        if clic[0] == True:
                            i=0
                        elif clic[1] == True:
                            i=1
                        elif clic[2] == True:
                            i=2
                        elif clic[3] == True:
                            i=3
                        elif clic[4] == True:
                            i=4
                        elif clic[5] == True:
                            i=5
                    elif i == 3:
                        if clic[0] == True:
                            i=0
                        elif clic[1] == True:
                            i=1
                        elif clic[2] == True:
                            i=2
                        elif clic[3] == True:
                            i=3
                        elif clic[4] == True:
                            i=4
                        elif clic[5] == True:
                            i=5
                    elif i == 4:
                        if clic[0] == True:
                            i=0
                        elif clic[1] == True:
                            i=1
                        elif clic[2] == True:
                            i=2
                        elif clic[3] == True:
                            i=3
                        elif clic[4] == True:
                            i=4
                        elif clic[5] == True:
                            i=5
                    elif i == 5 : 
                        if clic[0] == True:
                            i=0
                        elif clic[1] == True:
                            i=1
                        elif clic[2] == True:
                            i=2
                        elif clic[3] == True:
                            i=3
                        elif clic[4] == True:
                            i=4
                        elif clic[5] == True:
                            i=5
                    elif i == 6 :
                        if clic[2]==True:
                            i=9
                            client.socket.sendall("login".encode())
                            pseudo = ""
                            email=""
                            mdp=""
                            clic = [False]*len(menus[i].elementClic)
                        elif clic[1] == True:
                            pseudo = ""
                            email = ""
                            mdp = ""
                            client.socket.sendall("signup".encode())
                            i = 7
                            clic = [False]*len(menus[i].elementClic)
                    elif i == 7:
                        text = police.render(pseudo, True, (0,0,0))
                        textMdp = police.render(mdp, True, (0,0,0))
                        textEmail = police.render(email, True, (0,0,0))
                        pseudoE = False
                        emailE = False
                        mdpE = False
                        error = None
                        listeCaractere = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','0', '1', '2', '3',
                                        '4', '5', '6', '7', '8', '9']
                        rects = [pygame.Rect(853,405,472,99),pygame.Rect(853,567,472,99),pygame.Rect(853,729,472,99)]
                        while True:
                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    client.socket.close()
                                    pygame.quit()
                                    sys.exit()
                                if event.type == pygame.MOUSEBUTTONDOWN :
                                    if pygame.mouse.get_pressed()[0] :
                                        x,y = pygame.mouse.get_pos()
                                        clic = menus[i].clic(x,y)
                                if rects[0].collidepoint(x,y) or pseudoE == True:
                                        
                                    pseudoE = True
                                    mdpE = False
                                    emailE = False
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            pseudo= pseudo[:-1]
                                            text = police.render(pseudo, True,  (0,0,0),(0,0,0))  

                                    if event.type == pygame.TEXTINPUT and len(pseudo) <= 14:
                                        if event.text in listeCaractere:
                                            pseudo += event.text
                                            text = police.render(pseudo, True,  (0,0,0))
                                
                                if rects[1].collidepoint(x,y) or emailE == True:
                                        
                                    pseudoE = False
                                    mdpE = False
                                    emailE = True
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            email= email[:-1]
                                            textEmail = police.render(email, True,  (0,0,0))   

                                    if event.type == pygame.TEXTINPUT:
                                            email += event.text
                                            textEmail = police.render(email, True,  (0,0,0))
                                            
                                            
                                        
                                if rects[2].collidepoint(x,y) or mdpE == True:
                                    pseudoE = False
                                    emailE = False
                                    mdpE = True
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            mdp= mdp[:-1]
                                            textMdp = police.render(mdp, True,  (0,0,0))
                                                
                                    if event.type == pygame.TEXTINPUT:
                                        mdp += event.text
                                    textMdp = police.render("*"*len(mdp), True,  (0,0,0))           
                                    
                            x,y = pygame.mouse.get_pos()    
                            k = menus[i].hover(x,y)
                            fenetre.fill((0,0,0))
                            menus[i].afficherMenu(k)
                            fenetre.blit(text, (870, 425))
                            fenetre.blit(textEmail , (870, 590))
                            fenetre.blit(textMdp , (870, 750))
                            if error:
                                rendered_text = pygame.font.Font(None, 32).render(error, True, (255,255,255))
                                fenetre.blit(rendered_text, (1360,680))
                            pygame.display.flip()
                            if clic[0] == True:
                                i=8
                                break
                            elif clic[2]==True:
                                infos = pseudo+","+email+","+mdp
                                client.socket.sendall(infos.encode())
                                data = client.socket.recv(1024)
                                data = data.decode()
                                if data == "Success":
                                    pseudo = ""
                                    email = ""
                                    mdp = ""
                                    error = None
                                    i = 0
                                    break
                                else : 
                                    rendered_text = pygame.font.Font(None, 32).render(data, True, (255,255,255))
                                    fenetre.blit(rendered_text, (1360,680))
                                    pygame.display.update()
                                    error = data
                            elif clic[1] == True:
                                client.socket.sendall(b"exit")
                                i=6
                                break
                            clic = [False]*len(menus[i].elementClic)
                    elif i == 8:
                        pseudoE = False
                        emailE = False
                        mdpE = False
                        listeCaractere = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','0', '1', '2', '3',
                                        '4', '5', '6', '7', '8', '9']
                        rects = [pygame.Rect(853,405,472,99),pygame.Rect(853,567,472,99),pygame.Rect(853,729,472,99)]
                        while True:
                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    client.socket.close()
                                    pygame.quit()
                                    sys.exit()
                                if event.type == pygame.MOUSEBUTTONDOWN :
                                    if pygame.mouse.get_pressed()[0] :
                                        x,y = pygame.mouse.get_pos()
                                        clic = menus[i].clic(x,y)
                                if rects[0].collidepoint(x,y) or pseudoE == True:
                                        
                                    pseudoE = True
                                    mdpE = False
                                    emailE = False
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            pseudo= pseudo[:-1]
                                            text = police.render(pseudo, True,  (0,0,0),(0,0,0))  

                                    if event.type == pygame.TEXTINPUT and len(pseudo) <= 14:
                                        if event.text in listeCaractere:
                                            pseudo += event.text
                                            text = police.render(pseudo, True,  (0,0,0))
                                
                                if rects[1].collidepoint(x,y) or emailE == True:
                                        
                                    pseudoE = False
                                    emailE = True
                                    mdpE = False
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            email= email[:-1]
                                            textEmail = police.render(email, True,  (0,0,0))   

                                    if event.type == pygame.TEXTINPUT:
                                            email += event.text
                                            textEmail = police.render(email, True,  (0,0,0))
                                            
                                            
                                        
                                if rects[2].collidepoint(x,y) or mdpE == True:
                                    pseudoE = False
                                    emailE = False
                                    mdpE = True
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            mdp= mdp[:-1]
                                            textMdp = police.render(mdp, True,  (0,0,0))
                                                
                                    if event.type == pygame.TEXTINPUT:
                                        mdp += event.text
                                    textMdp = police.render(mdp, True,  (0,0,0))           
                            
                            x,y = pygame.mouse.get_pos()    
                            k = menus[i].hover(x,y)
                            fenetre.fill((0,0,0))
                            menus[i].afficherMenu(k)
                            if error:
                                rendered_text = pygame.font.Font(None, 32).render(data, True, (255,255,255))
                                fenetre.blit(rendered_text, (1360,680))
                            fenetre.blit(text, (870, 425))
                            fenetre.blit(textEmail , (870, 590))
                            fenetre.blit(textMdp , (870, 750))
                            pygame.display.flip()
                            if clic[0] == True:
                                i=7
                                break
                            elif clic[2]==True:
                                infos = pseudo+","+email+","+mdp
                                client.socket.sendall(infos.encode())
                                data = client.socket.recv(1024)
                                data = data.decode()
                                if data == "Success":
                                    pseudo = ""
                                    email = ""
                                    mdp = ""  
                                    error = None      
                                    i = 0
                                else : 
                                    rendered_text = pygame.font.Font(None, 32).render(data, True, (255,255,255))
                                    fenetre.blit(rendered_text, (1360,680))
                                    pygame.display.update()
                            elif clic[1] == True:
                                client.socket.send(b"exit")
                                i=6
                            clic = [False]*len(menus[i].elementClic)
                    elif i == 9:
                        text = police.render(pseudo, True, (0,0,0))
                        textMdp = police.render(mdp, True, (0,0,0))
                        pseudoE = False
                        mdpE = False
                        error = None
                        listeCaractere = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','0', '1', '2', '3',
                                        '4', '5', '6', '7', '8', '9']
                        rects = [pygame.Rect(853,405,472,99),pygame.Rect(853,577,472,99)]
                        while True:
                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    client.socket.close()
                                    pygame.quit()
                                    sys.exit()
                                if event.type == pygame.MOUSEBUTTONDOWN :
                                    if pygame.mouse.get_pressed()[0] :
                                        x,y = pygame.mouse.get_pos()
                                        clic = menus[i].clic(x,y)
                                if rects[0].collidepoint(x,y) or pseudoE == True:
                                        
                                    pseudoE = True
                                    mdpE = False
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            pseudo= pseudo[:-1]
                                            text = police.render(pseudo, True,  (0,0,0),(0,0,0))  

                                    if event.type == pygame.TEXTINPUT:
                                        if event.text in listeCaractere:
                                            pseudo += event.text
                                            text = police.render(pseudo, True,  (0,0,0))
                                            
                                            
                                if rects[1].collidepoint(x,y) or mdpE == True:
                                    pseudoE = False
                                    mdpE = True
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            mdp= mdp[:-1]
                                            textMdp = police.render(mdp, True,  (0,0,0))
                                                
                                    if event.type == pygame.TEXTINPUT:
                                        mdp += event.text
                                    textMdp = police.render("*"*len(mdp), True,  (0,0,0))           
                                    
                            x,y = pygame.mouse.get_pos()    
                            k = menus[i].hover(x,y)
                            fenetre.fill((0,0,0))
                            menus[i].afficherMenu(k)
                            fenetre.blit(text, (870, 425))
                            fenetre.blit(textMdp , (870, 590))
                            if error:
                                rendered_text = pygame.font.Font(None, 32).render(error, True, (255,255,255))
                                fenetre.blit(rendered_text, (1360,680))
                            pygame.display.flip()
                            if clic[0] == True:
                                i=10
                                break
                            elif clic[2]==True:
                                infos = pseudo+","+mdp
                                client.socket.sendall(infos.encode())
                                data = client.socket.recv(1024)
                                data = data.decode()
                                if data == "Success":
                                    pseudo = ""
                                    mdp = ""
                                    error = None
                                    i = 0
                                    break
                                else : 
                                    rendered_text = pygame.font.Font(None, 32).render(data, True, (255,255,255))
                                    fenetre.blit(rendered_text, (1360,680))
                                    pygame.display.update()
                                    error = data
                            elif clic[1] == True:
                                client.socket.sendall(b"exit")
                                i=6
                                break
                            clic = [False]*len(menus[i].elementClic)
                    elif i ==10:
                        pseudoE = False
                        mdpE = False
                        listeCaractere = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                                        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z','0', '1', '2', '3',
                                        '4', '5', '6', '7', '8', '9']
                        rects = [pygame.Rect(853,405,472,99),pygame.Rect(853,577,472,99)]
                        while True:
                            for event in pygame.event.get():
                                if event.type == pygame.QUIT:
                                    client.socket.close()
                                    pygame.quit()
                                    sys.exit()
                                if event.type == pygame.MOUSEBUTTONDOWN :
                                    if pygame.mouse.get_pressed()[0] :
                                        x,y = pygame.mouse.get_pos()
                                        clic = menus[i].clic(x,y)
                                if rects[0].collidepoint(x,y) or pseudoE == True:
                                        
                                    pseudoE = True
                                    mdpE = False
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            pseudo= pseudo[:-1]
                                            text = police.render(pseudo, True,  (0,0,0),(0,0,0))  

                                    if event.type == pygame.TEXTINPUT:
                                        if event.text in listeCaractere:
                                            pseudo += event.text
                                            text = police.render(pseudo, True,  (0,0,0))
                                
                            
                                        
                                if rects[1].collidepoint(x,y) or mdpE == True:
                                    pseudoE = False
                                    mdpE = True
                                    if event.type == pygame.KEYDOWN:
                                        if event.key == pygame.K_BACKSPACE:
                                            mdp= mdp[:-1]
                                            textMdp = police.render(mdp, True,  (0,0,0))
                                                
                                    if event.type == pygame.TEXTINPUT:
                                        mdp += event.text
                                    textMdp = police.render(mdp, True,  (0,0,0))           
                            
                            x,y = pygame.mouse.get_pos()
                            k = menus[i].hover(x,y)
                            fenetre.fill((0,0,0))
                            menus[i].afficherMenu(k)
                            if error:
                                rendered_text = pygame.font.Font(None, 32).render(data, True, (255,255,255))
                                fenetre.blit(rendered_text, (1360,680))
                            fenetre.blit(text, (870, 425))
                            fenetre.blit(textMdp , (870, 590))
                            pygame.display.flip()
                            if clic[0] == True:
                                i=9
                                break
                            elif clic[2]==True:
                                infos = pseudo+","+mdp
                                client.socket.sendall(infos.encode())
                                data = client.socket.recv(1024)
                                data = data.decode()
                                if data == "Success":
                                    pseudo = ""
                                    email = ""
                                    mdp = ""  
                                    error = None      
                                    i = 0
                                else : 
                                    rendered_text = pygame.font.Font(None, 32).render(data, True, (255,255,255))
                                    fenetre.blit(rendered_text, (1360,680))
                                    pygame.display.update()
                            elif clic[1] == True:
                                client.socket.send(b"exit")
                                i=6
                            clic = [False]*len(menus[i].elementClic)
                    elif i == -1:
                        if event.type == pygame.MOUSEBUTTONDOWN :
                            if pygame.mouse.get_pressed()[0] :
                                i=0
            if i not in [7,8,9,10]:      
                x,y = pygame.mouse.get_pos()
                if i!=-1:
                    k = menus[i].hover(x,y)
                fenetre.fill((0,0,0))
                menus[i].afficherMenu(k)
                pygame.display.flip()
            clock.tick(60)

if __name__ == "__main__":

    main()