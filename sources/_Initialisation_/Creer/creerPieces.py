from _Initialisation_.Creer.MesClasses.pieces import *
from Fonctions.Deplacer.modifierCases import *
import pygame

def creerPieces():
    """
    initialise les pieces
    """
    boitePieces = BoitePieces()
    pieces=boitePieces.pieces
    posempbase=[(1,6),(10,6)]
    possolbase=[[(1,1),(1,2),(1,3),(1,4),(1,5),(1,7),(1,8),(1,9),(1,10),
                 (2,1),(2,2),(2,3),(2,4),(2,5),(2,6),(2,7),(2,8),(2,9),(2,10)],
                 [(10,1),(10,2),(10,3),(10,4),(10,5),(10,7),(10,8),(10,9),(10,10),
                 (9,1),(9,2),(9,3),(9,4),(9,5),(9,6),(9,7),(9,8),(9,9),(9,10)]]
    for g in range (2):
        gardien = Gardien(g+1,posempbase[g])
        pieces=boitePieces.add_piece(gardien,pieces)
        for s in range (19):
            soldat = Soldat(g+1,possolbase[g][s])
            pieces=boitePieces.add_piece(soldat,pieces)
    return boitePieces,pieces

def creerPiecesFusion(caseclic2,boitePiece,joueur,Canfusion,pieces,pieceafusionner,pieceasupprimer,
                                       ancpos,newpos,cases,images_jeu,fenetrejeu,canPlay):
    """
    crée la piece résultant de la fusion
    """
    niveau=Canfusion[1]+1
    if niveau!=4:
        sousniv=choixFusion(niveau,caseclic2,joueur,images_jeu,fenetrejeu)
        if sousniv==None:
            return pieces,cases,canPlay
        if sousniv==0:
            if niveau==2:
                bastion=Bastion(joueur,(caseclic2[1],caseclic2[0]))
                boitePiece.add_piece(bastion,pieces)
                pieces=boitePiece.pieces
            elif niveau==3:
                donjon=Donjon(joueur,(caseclic2[1],caseclic2[0]))
                boitePiece.add_piece(donjon,pieces)
                pieces=boitePiece.pieces
        elif sousniv==1:
            if niveau==2:
                jumpeur=Jumper(joueur,(caseclic2[1],caseclic2[0]))
                boitePiece.add_piece(jumpeur,pieces)
                pieces=boitePiece.pieces
            elif niveau==3:
                chapelain=Chapelain(joueur,(caseclic2[1],caseclic2[0]))
                boitePiece.add_piece(chapelain,pieces)
                pieces=boitePiece.pieces
        canPlay=False
        

    else:
        empereur=Empereur(joueur,(caseclic2[1],caseclic2[0]))
        boitePiece.add_piece(empereur,pieces)
        pieces=boitePiece.pieces
        canPlay=False
    
    cases=modifierCases(cases,ancpos,newpos,pieceasupprimer)
    pieces=boitePiece.remove_piece(pieceafusionner,pieces)
    pieces=boitePiece.remove_piece(pieceasupprimer,pieces)
           
    return pieces,cases,canPlay

def choixFusion(niveau,caseclic2,joueur,images_jeu,fenetrejeu):
    """gère le choix de la fusion
    """
    clic=False
    xc=600+((caseclic2[0]-1)*80)
    yc=940-(caseclic2[1]*80)
    if niveau==2:
        if joueur==1:
            fenetrejeu.blit(images_jeu[10],(xc-80,yc-80))
            fenetrejeu.blit(images_jeu[3][1],(xc-70,yc-70))
            fenetrejeu.blit(images_jeu[4][1],(xc+10,yc-70))
        elif joueur==2:
            fenetrejeu.blit(images_jeu[10],(1840-xc,920-yc))
            fenetrejeu.blit(images_jeu[3][0],(1850-xc,930-yc))
            fenetrejeu.blit(images_jeu[4][0],(1930-xc,930-yc))
    elif niveau==3:
        if joueur==1:
            fenetrejeu.blit(images_jeu[10],(xc-80,yc-80))
            fenetrejeu.blit(images_jeu[5][1],(xc-70,yc-70))
            fenetrejeu.blit(images_jeu[6][1],(xc+10,yc-70))
        elif joueur==2:
            fenetrejeu.blit(images_jeu[10],(1840-xc,920-yc))
            fenetrejeu.blit(images_jeu[5][0],(1850-xc,930-yc))
            fenetrejeu.blit(images_jeu[6][0],(1930-xc,930-yc))
    pygame.display.update()
    while clic==False:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                if pygame.mouse.get_pressed()[0]:
                    x,y = pygame.mouse.get_pos()
                    clic=True
    sousniv=choixPieceFusion(joueur,x,y,xc,yc)
    return sousniv


def choixPieceFusion(joueur,x,y,xc,yc):
    """
    ditle sous niveua du choix selon le clic
    """
    if joueur==1:
        if y>yc-80 and y<yc:
            if x>xc-80 and x<xc+80:
                if x>xc-80 and x<xc:
                    return 0
                else:
                    return 1
    elif joueur==2:
        if y<1000-yc and y>920-yc:
            if x>1840-xc and x<2000-xc :
                if x>1840-xc and x<1920-xc:
                    return 0
                else:
                    return 1