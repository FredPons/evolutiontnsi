def creerCases():
    """crée la liste des cases
    """

    cases=[]
    for i in range(10):
        casex=[]
        for j in range(10):
            if i<=1:
                casex.append((True,1))
            elif i>=8:
                casex.append((True,2))
            else:
                casex.append((False,0))
        cases.append(casex)
    return cases