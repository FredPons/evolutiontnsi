class BoitePieces:

    def __init__(self):
        self.pieces=[[[]],[[]],[[],[]],[[],[]],[[]]]

    def add_piece(self, piece,pieces):
        self.pieces=pieces
        niveau = piece.niveau
        sousniveau = piece.sousniveau
        self.pieces[niveau][sousniveau].append(piece)
        return self.pieces
    
    def remove_piece(self, piece,pieces):
        self.pieces=pieces
        niveau = piece.niveau
        sousniveau = piece.sousniveau
        index = self.pieces[niveau][sousniveau].index(piece)
        del self.pieces[niveau][sousniveau][index]
        return self.pieces

class Gardien:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 0
        self.sousniveau = 0
        self.couleur = couleur
        self.reliee = False
    
    def deplacer(self,newpos):
        self.position = newpos


    def __repr__(self):
        """Représentation de l'objet."""
        return f"G au joueur {self.couleur} en {self.position}"

class Soldat:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 1
        self.sousniveau = 0
        self.couleur = couleur
    
    def deplacer(self,newpos):
        self.position = newpos

    def __repr__(self):
        """Représentation de l'objet."""
        return f"S au joueur {self.couleur} en {self.position}"

class Bastion:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 2
        self.sousniveau = 0
        self.couleur = couleur
    
    def deplacer(self,newpos):
        self.position = newpos

    def __repr__(self):
        """Représentation de l'objet."""
        return f"B au joueur {self.couleur} en {self.position}"

class Jumper:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 2
        self.sousniveau = 1
        self.couleur = couleur
    
    def deplacer(self,newpos):
        self.position = newpos

    def __repr__(self):
        """Représentation de l'objet."""
        return f"J au joueur {self.couleur} en {self.position}"

class Donjon:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 3
        self.sousniveau = 0
        self.couleur = couleur
    
    def deplacer(self,newpos):
        self.position = newpos

    def __repr__(self):
        """Représentation de l'objet."""
        return f"D au joueur {self.couleur} en {self.position}"

class Chapelain:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 3
        self.sousniveau = 1
        self.couleur = couleur
    
    def deplacer(self,newpos):
        self.position = newpos

    def __repr__(self):
        """Représentation de l'objet."""
        return f"C au joueur {self.couleur} en {self.position}"

class Empereur:

    def __init__(self,couleur,position):
        self.position = position
        self.niveau = 4
        self.sousniveau = 0
        self.couleur = couleur
    
    def deplacer(self,newpos):
        self.position = newpos

    def __repr__(self):
        """Représentation de l'objet."""
        return f"E au joueur {self.couleur} en {self.position}"
    
class Banc:

    def __init__(self):
        self.piecesshop=[]

    def add_piece(self,piece,piecesshop):
        self.piecesshop=piecesshop
        self.piecesshop.append(piece)
        return self.piecesshop
    
    def remove_piece(self, piece,piecesshop):
        self.piecesshop=piecesshop
        self.piecesshop.remove(piece)
        return self.piecesshop