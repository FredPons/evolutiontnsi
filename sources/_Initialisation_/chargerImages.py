def chargerImages():
    """
                charge les images depusi le fichier
    """
    import os,pygame
    images_fichier = os.path.join(os.path.dirname(__file__), 'Images')
    plateau = os.path.join(images_fichier, 'plateau.png')
    gardienn = os.path.join(images_fichier, 'gardienn.png')
    gardienr = os.path.join(images_fichier, 'gardienr.png')
    soldatn = os.path.join(images_fichier, 'soldatn.png')
    soldatr = os.path.join(images_fichier, 'soldatr.png')
    bastionn = os.path.join(images_fichier, 'bastionn.png')
    bastionr = os.path.join(images_fichier, 'bastionr.png')
    jumpern = os.path.join(images_fichier, 'jumpern.png')
    jumperr = os.path.join(images_fichier, 'jumperr.png')
    donjonn = os.path.join(images_fichier, 'donjonn.png')
    donjonr = os.path.join(images_fichier, 'donjonr.png')
    chapelainn = os.path.join(images_fichier, 'chapelainn.png')
    chapelainr = os.path.join(images_fichier, 'chapelainr.png')
    empereurn = os.path.join(images_fichier, 'empereurn.png')
    empereurr = os.path.join(images_fichier, 'empereurr.png')
    ptVert = os.path.join(images_fichier, '_pointVert_.png')
    ptRose = os.path.join(images_fichier, '_pointRose_.png')
    ptOrange = os.path.join(images_fichier, '_pointOrange_.png')
    FondFusion=os.path.join(images_fichier, 'FondFusion.png')
    danger=os.path.join(images_fichier, 'danger.png')
    grab=os.path.join(images_fichier, 'grab.png')
    banc=os.path.join(images_fichier, 'banc.png')
    poubelle=os.path.join(images_fichier, 'poubelle.png')
    FondChoix=os.path.join(images_fichier, 'FondChoix.png')
    refresh=os.path.join(images_fichier, 'refresh.png')

    image_jeu=[pygame.image.load(plateau),[pygame.image.load(gardienn),pygame.image.load(gardienr)],
            [pygame.image.load(soldatn),pygame.image.load(soldatr)],
            [pygame.image.load(bastionn),pygame.image.load(bastionr)],
            [pygame.image.load(jumpern),pygame.image.load(jumperr)],
            [pygame.image.load(donjonn),pygame.image.load(donjonr)],
            [pygame.image.load(chapelainn),pygame.image.load(chapelainr)],
            [pygame.image.load(empereurn),pygame.image.load(empereurr)],
            pygame.image.load(ptVert),pygame.image.load(ptRose),pygame.image.load(FondFusion),
            pygame.image.load(danger),pygame.image.load(grab),pygame.image.load(banc),
            pygame.image.load(poubelle),pygame.image.load(FondChoix),pygame.image.load(refresh),
            pygame.image.load(ptOrange)]
    return image_jeu
