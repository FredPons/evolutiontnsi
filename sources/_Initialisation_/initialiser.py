def initialiser():
    """initialise les boites, pieces et cases"""
    from _Initialisation_.Creer.creerCases import creerCases
    from _Initialisation_.Creer.creerPieces import creerPieces
    
    cases=creerCases()
    boitePieces,pieces=creerPieces()

    return boitePieces,cases,pieces