def determinerJoueur(joueur):
    """change le nom de joueur en numéro"""
    joueur = joueur.decode()
    if joueur == "p1":
        joueur = 1
    elif joueur == "p2":
        joueur = 2
    return joueur
