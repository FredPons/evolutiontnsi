def casesPieceCanMove(caseclic,pieces,joueur):
    """regarde les cases où on peut aller selon la case cliquée

    Args:
        caseclic (tuple): case cliquée
        pieces (list): liste des pieces
        joueur (int): numero du joueur


    """

    Canfusion=[0,0]
    caseCanMove=[]
    caseGrab=[]
    pieceGrab=None
    for niveau in pieces:
        for sousniveau in niveau:
            for piece in sousniveau:
                if piece.position[0]==caseclic[1] and piece.position[1]==caseclic[0]:
                    piecebase=piece
    for gardien in pieces[0][0]:
        if gardien.position[0] == caseclic[1] and gardien.position[1] == caseclic[0]:
            return caseCanMove,Canfusion,piecebase,caseGrab,pieceGrab
    
    for soldat in pieces[1][0]:
        if soldat.position[0] == caseclic[1] and soldat.position[1] == caseclic[0]:
            caseCanMove = [(caseclic[0]+1,caseclic[1]+1),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),
                (caseclic[0]-1,caseclic[1]+1)]
            case_pour_grab = [(caseclic[0],caseclic[1]+1),
                (caseclic[0]+1,caseclic[1]),(caseclic[0],caseclic[1]-1),
                (caseclic[0]-1,caseclic[1])]
                
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in case_pour_grab:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del case_pour_grab[c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        for gardien in pieces[0][0]:
                                            if gardien==piece :
                                                caseGrab.append(case)
                                                pieceGrab=piecebase
                                        caseAsupprimer.append(i)
                i += 1

            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in case_pour_grab:
                for gardien in pieces[0][0]:
                    if gardien.couleur==piecebase.couleur:
                        if gardien.position[1]==case[0] and gardien.position[0]==case[1]:
                            caseGrab.append(case)
                            pieceGrab=piecebase

            for case in caseCanMove:
                for gardien in pieces[0][0]:
                    if gardien.couleur!=joueur:
                        if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                            caseCanMove.remove(case)

    for bastion in pieces[2][0]:
        if bastion.position[0] == caseclic[1] and bastion.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1])],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1])]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i) 
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case0=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case0=caseclic[0]+2
                                    elif case[0]==caseclic[0]-1:
                                        case0=caseclic[0]-2
                                    if case[1]==caseclic[1]:
                                        case1=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case1=caseclic[1]+2
                                    elif case[1]==caseclic[1]-1:
                                        case1=caseclic[1]-2
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case0,case1):
                                            caseCanMove[1].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case0=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case0=caseclic[0]+2
                                        elif case[0]==caseclic[0]-1:
                                            case0=caseclic[0]-2
                                        if case[1]==caseclic[1]:
                                            case1=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case1=caseclic[1]+2
                                        elif case[1]==caseclic[1]-1:
                                            case1=caseclic[1]-2
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case0,case1):
                                                caseCanMove[1].remove(casesupp)
        
    for donjon in pieces[3][0]:
        if donjon.position[0] == caseclic[1] and donjon.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1),(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1)],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2),(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2)],[(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3),(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case01,case11):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
            
            caseCanMove[0]=[]
        
    for empereur in pieces[4][0]:
        if empereur.position[0] == caseclic[1] and empereur.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1)],[(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2),(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2)],[(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3),(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3)],[(caseclic[0],caseclic[1]+4),(caseclic[0]+4,caseclic[1]+4),(caseclic[0]+4,caseclic[1]),
                (caseclic[0]+4,caseclic[1]-4),(caseclic[0],caseclic[1]-4),(caseclic[0]-4,caseclic[1]-4),(caseclic[0]-4,caseclic[1]),
                (caseclic[0]-4,caseclic[1]+4)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+3:
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-3:
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+3:
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-3:
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+3:
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-3:
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+3:
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-3:
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

    for jumper in pieces[2][1]:
        if jumper.position[0] == caseclic[1] and jumper.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+1)],[(caseclic[0],caseclic[1]+2)],
                            [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1)]],
                            [[(caseclic[0]+1,caseclic[1])],[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),
                                (caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]-1)]],
                            [[(caseclic[0],caseclic[1]-1)],[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),
                                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1)]],
                            [[(caseclic[0]-1,caseclic[1])],[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]-1,caseclic[1]-1)]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau==piecebase.sousniveau:
                                                Canfusion=[Canfusion[0]+1,piece.niveau]
                                            else:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[1][0]
                sautcase=types[0][0]
                if len(types[2])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True

                if sup==False:
                    saut=False
                    for niveau in pieces:
                        if saut==False:
                            for sousniveau in niveau:
                                if saut==False:
                                    for piece in sousniveau:
                                        if saut==False:
                                            if piece.position[1]==sautcase[0] and piece.position[0]==sautcase[1]:
                                                saut=True
                    if saut==False:
                        typesAsupprimer.append(i)
                        sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[[],[[],[],[],[]]]
            i=0
            for type in casesprov:
                for case in type[2]:
                    caseCanMove[1][i].append(case)
                for case in type[1]:
                    caseCanMove[0].append(case)
                i+=1

    for chapelain in pieces[3][1]:
        if chapelain.position[0] == caseclic[1] and chapelain.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+2)],
                           [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),(caseclic[0],caseclic[1]+1)]],
                            [[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),(caseclic[0]+1,caseclic[1])]],
                            [[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),(caseclic[0],caseclic[1]-1)]],
                            [[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),(caseclic[0]-1,caseclic[1])]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau==piecebase.sousniveau:
                                                Canfusion=[Canfusion[0]+1,piece.niveau]
                                            else:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[0][0]
                if len(types[1])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[[],[[],[],[],[]]]
            i=0
            for type in casesprov:
                for case in type[1]:
                    caseCanMove[1][i].append(case)
                for case in type[0]:
                    caseCanMove[0].append(case)
                i+=1
    return caseCanMove,Canfusion,piecebase,caseGrab,pieceGrab

def casesPieceCanMoveGrab(caseclic,pieces,joueur,Grab):
    Canfusion=[0,0]
    caseCanMove=[]
    caseGrab=[]
    pieceGrab=None
    ok=False
    for niveau in pieces:
        for sousniveau in niveau:
            for piece in sousniveau:
                if piece.position[0]==caseclic[1] and piece.position[1]==caseclic[0]:
                    piecebase=piece
    for gardien in pieces[0][0]:
        if gardien.position[0] == caseclic[1] and gardien.position[1] == caseclic[0]:
            return caseCanMove,Canfusion,piecebase,caseGrab,pieceGrab,ok
    
    for soldat in pieces[1][0]:
        if soldat.position[0] == caseclic[1] and soldat.position[1] == caseclic[0]:
            caseCanMove = [(caseclic[0]+1,caseclic[1]+1),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),
                (caseclic[0]-1,caseclic[1]+1)]
            case_pour_grab = [(caseclic[0],caseclic[1]+1),
                (caseclic[0]+1,caseclic[1]),(caseclic[0],caseclic[1]-1),
                (caseclic[0]-1,caseclic[1])]
                
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in case_pour_grab:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del case_pour_grab[c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau and piecebase.position!=Grab[joueur-1][1].position:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        for gardien in pieces[0][0]:
                                            if gardien==piece :
                                                caseGrab.append(case)
                                                pieceGrab=piecebase
                                        caseAsupprimer.append(i)
                i += 1

            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in case_pour_grab:
                for gardien in pieces[0][0]:
                    if gardien.couleur==piecebase.couleur:
                        if gardien.position[1]==case[0] and gardien.position[0]==case[1]:
                            caseGrab.append(case)
                            pieceGrab=piecebase

            for case in caseCanMove:
                for gardien in pieces[0][0]:
                    if gardien.couleur!=joueur:
                        if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                            caseCanMove.remove(case)

    for bastion in pieces[2][0]:
        if bastion.position[0] == caseclic[1] and bastion.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1])],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1])]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i) 
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case0=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case0=caseclic[0]+2
                                    elif case[0]==caseclic[0]-1:
                                        case0=caseclic[0]-2
                                    if case[1]==caseclic[1]:
                                        case1=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case1=caseclic[1]+2
                                    elif case[1]==caseclic[1]-1:
                                        case1=caseclic[1]-2
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case0,case1):
                                            caseCanMove[1].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case0=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case0=caseclic[0]+2
                                        elif case[0]==caseclic[0]-1:
                                            case0=caseclic[0]-2
                                        if case[1]==caseclic[1]:
                                            case1=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case1=caseclic[1]+2
                                        elif case[1]==caseclic[1]-1:
                                            case1=caseclic[1]-2
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case0,case1):
                                                caseCanMove[1].remove(casesupp)
        
    for donjon in pieces[3][0]:
        if donjon.position[0] == caseclic[1] and donjon.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1),(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1)],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2),(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2)],[(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3),(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau==piecebase.sousniveau:
                                            Canfusion=[Canfusion[0]+1,piece.niveau]
                                        else:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case01,case11):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
            caseCanMove[0]=[]

    for empereur in pieces[4][0]:
        if empereur.position[0] == caseclic[1] and empereur.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1)],[(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2),(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2)],[(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3),(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3)],[(caseclic[0],caseclic[1]+4),(caseclic[0]+4,caseclic[1]+4),(caseclic[0]+4,caseclic[1]),
                (caseclic[0]+4,caseclic[1]-4),(caseclic[0],caseclic[1]-4),(caseclic[0]-4,caseclic[1]-4),(caseclic[0]-4,caseclic[1]),
                (caseclic[0]-4,caseclic[1]+4)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+3:
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-3:
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+3:
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-3:
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+3:
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-3:
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+3:
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-3:
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

    for jumper in pieces[2][1]:
        if jumper.position[0] == caseclic[1] and jumper.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+1)],[(caseclic[0],caseclic[1]+2)],
                            [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1)]],
                            [[(caseclic[0]+1,caseclic[1])],[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),
                                (caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]-1)]],
                            [[(caseclic[0],caseclic[1]-1)],[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),
                                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1)]],
                            [[(caseclic[0]-1,caseclic[1])],[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]-1,caseclic[1]-1)]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau==piecebase.sousniveau:
                                                Canfusion=[Canfusion[0]+1,piece.niveau]
                                            else:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[1][0]
                sautcase=types[0][0]
                if len(types[2])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True

                if sup==False:
                    saut=False
                    for niveau in pieces:
                        if saut==False:
                            for sousniveau in niveau:
                                if saut==False:
                                    for piece in sousniveau:
                                        if saut==False:
                                            if piece.position[1]==sautcase[0] and piece.position[0]==sautcase[1]:
                                                saut=True
                    if saut==False:
                        typesAsupprimer.append(i)
                        sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[[],[[],[],[],[]]]
            i=0
            for type in casesprov:
                for case in type[2]:
                    caseCanMove[1][i].append(case)
                for case in type[1]:
                    caseCanMove[0].append(case)
                i+=1

    for chapelain in pieces[3][1]:
        if chapelain.position[0] == caseclic[1] and chapelain.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+2)],
                           [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),(caseclic[0],caseclic[1]+1)]],
                            [[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),(caseclic[0]+1,caseclic[1])]],
                            [[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),(caseclic[0],caseclic[1]-1)]],
                            [[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),(caseclic[0]-1,caseclic[1])]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau==piecebase.sousniveau:
                                                Canfusion=[Canfusion[0]+1,piece.niveau]
                                            else:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[0][0]
                if len(types[1])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[[],[[],[],[],[]]]
            i=0
            for type in casesprov:
                for case in type[1]:
                    caseCanMove[1][i].append(case)
                for case in type[0]:
                    caseCanMove[0].append(case)
                i+=1
    
    if Grab[joueur-1][1]==piecebase:
        ok=True
    return caseCanMove,Canfusion,piecebase,caseGrab,pieceGrab,ok

def casesCanRefresh(pieces,joueur,cases):
    """
    détermine les cases où on peut refresh
    """
    for gardien in pieces[0][0]:
        if gardien.couleur==joueur:
            caseclic=(gardien.position[1],gardien.position[0])
            caseRefresh = [(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                        (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),
                        (caseclic[0]-1,caseclic[1]),
                        (caseclic[0]-1,caseclic[1]+1)] 
    
    i=0
    caseAsupprimer=[]
    for case in caseRefresh:
        if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                caseAsupprimer.append(i)
        i += 1
    i=0
    for c in caseAsupprimer:
        del caseRefresh[c-i]
        i += 1
     
    c=0
    caseAsupprimer=[]    
    for case in caseRefresh:
        if cases[case[1]-1][case[0]-1][0]==True:
            caseAsupprimer.append(c)
        c += 1
    i=0
    for c in caseAsupprimer:
        del caseRefresh[c-i]
        i += 1
    
    return caseRefresh

def casesPieceCanMove2(piecebase,pieces,joueur):
    caseCanMove=[]
    caseclic=(piecebase.position[1],piecebase.position[0])
    for gardien in pieces[0][0]:
        if gardien.position[0] == caseclic[1] and gardien.position[1] == caseclic[0]:
            return caseCanMove
    
    for soldat in pieces[1][0]:
        if soldat.position[0] == caseclic[1] and soldat.position[1] == caseclic[0]:
            caseCanMove = [(caseclic[0]+1,caseclic[1]+1),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),
                (caseclic[0]-1,caseclic[1]+1)]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                i += 1

            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
            for case in caseCanMove:
                for gardien in pieces[0][0]:
                    if gardien.couleur!=joueur:
                        if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                            caseCanMove.remove(case)

    for bastion in pieces[2][0]:
        if bastion.position[0] == caseclic[1] and bastion.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1])],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1])]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i) 
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case0=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case0=caseclic[0]+2
                                    elif case[0]==caseclic[0]-1:
                                        case0=caseclic[0]-2
                                    if case[1]==caseclic[1]:
                                        case1=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case1=caseclic[1]+2
                                    elif case[1]==caseclic[1]-1:
                                        case1=caseclic[1]-2
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case0,case1):
                                            caseCanMove[1].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case0=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case0=caseclic[0]+2
                                        elif case[0]==caseclic[0]-1:
                                            case0=caseclic[0]-2
                                        if case[1]==caseclic[1]:
                                            case1=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case1=caseclic[1]+2
                                        elif case[1]==caseclic[1]-1:
                                            case1=caseclic[1]-2
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case0,case1):
                                                caseCanMove[1].remove(casesupp)
            caseok=caseCanMove
            caseCanMove=[]
            for surcase in caseok:
                for case in surcase:
                    caseCanMove.append(case)

    for donjon in pieces[3][0]:
        if donjon.position[0] == caseclic[1] and donjon.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1),(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1)],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2),(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2)],[(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3),(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case01,case11):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)

            caseCanMove[0]=[]
            caseok=caseCanMove
            caseCanMove=[]
            for surcase in caseok:
                for case in surcase:
                    caseCanMove.append(case)
        
    for empereur in pieces[4][0]:
        if empereur.position[0] == caseclic[1] and empereur.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1)],[(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2),(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2)],[(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3),(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3)],[(caseclic[0],caseclic[1]+4),(caseclic[0]+4,caseclic[1]+4),(caseclic[0]+4,caseclic[1]),
                (caseclic[0]+4,caseclic[1]-4),(caseclic[0],caseclic[1]-4),(caseclic[0]-4,caseclic[1]-4),(caseclic[0]-4,caseclic[1]),
                (caseclic[0]-4,caseclic[1]+4)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+3:
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-3:
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+3:
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-3:
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+3:
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-3:
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+3:
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-3:
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)
            caseok=caseCanMove
            caseCanMove=[]
            for surcase in caseok:
                for case in surcase:
                    caseCanMove.append(case)

    for jumper in pieces[2][1]:
        if jumper.position[0] == caseclic[1] and jumper.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+1)],[(caseclic[0],caseclic[1]+2)],
                            [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1)]],
                            [[(caseclic[0]+1,caseclic[1])],[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),
                                (caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]-1)]],
                            [[(caseclic[0],caseclic[1]-1)],[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),
                                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1)]],
                            [[(caseclic[0]-1,caseclic[1])],[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]-1,caseclic[1]-1)]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau!=piecebase.sousniveau:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[1][0]
                sautcase=types[0][0]
                if len(types[2])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True

                if sup==False:
                    saut=False
                    for niveau in pieces:
                        if saut==False:
                            for sousniveau in niveau:
                                if saut==False:
                                    for piece in sousniveau:
                                        if saut==False:
                                            if piece.position[1]==sautcase[0] and piece.position[0]==sautcase[1]:
                                                saut=True
                    if saut==False:
                        typesAsupprimer.append(i)
                        sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[]
            for type in casesprov:
                for case in type[2]:
                    caseCanMove.append(case)

    for chapelain in pieces[3][1]:
        if chapelain.position[0] == caseclic[1] and chapelain.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+2)],
                           [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),(caseclic[0],caseclic[1]+1)]],
                            [[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),(caseclic[0]+1,caseclic[1])]],
                            [[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),(caseclic[0],caseclic[1]-1)]],
                            [[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),(caseclic[0]-1,caseclic[1])]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau!=piecebase.sousniveau:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[0][0]
                if len(types[1])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[]
            for type in casesprov:
                for case in type[1]:
                    caseCanMove.append(case)
    return caseCanMove

def casesPieceCanMoveGrab2(piecebase,pieces,joueur,Grab):
    caseCanMove=[]
    ok=False
    caseclic=(piecebase.position[1],piecebase.position[0])

    for gardien in pieces[0][0]:
        if gardien.position[0] == caseclic[1] and gardien.position[1] == caseclic[0]:
            return caseCanMove,ok
    
    for soldat in pieces[1][0]:
        if soldat.position[0] == caseclic[1] and soldat.position[1] == caseclic[0]:
            caseCanMove = [(caseclic[0]+1,caseclic[1]+1),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),
                (caseclic[0]-1,caseclic[1]+1)]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau or piecebase.position==Grab[joueur-1][1].position:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                i += 1

            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
            for case in caseCanMove:
                for gardien in pieces[0][0]:
                    if gardien.couleur!=joueur:
                        if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                            caseCanMove.remove(case)

    for bastion in pieces[2][0]:
        if bastion.position[0] == caseclic[1] and bastion.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1])],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1])]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i) 
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case0=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case0=caseclic[0]+2
                                    elif case[0]==caseclic[0]-1:
                                        case0=caseclic[0]-2
                                    if case[1]==caseclic[1]:
                                        case1=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case1=caseclic[1]+2
                                    elif case[1]==caseclic[1]-1:
                                        case1=caseclic[1]-2
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case0,case1):
                                            caseCanMove[1].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case0=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case0=caseclic[0]+2
                                        elif case[0]==caseclic[0]-1:
                                            case0=caseclic[0]-2
                                        if case[1]==caseclic[1]:
                                            case1=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case1=caseclic[1]+2
                                        elif case[1]==caseclic[1]-1:
                                            case1=caseclic[1]-2
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case0,case1):
                                                caseCanMove[1].remove(casesupp)
            
            caseok=caseCanMove
            caseCanMove=[]
            for surcase in caseok:
                for case in surcase:
                    caseCanMove.append(case)
        
    for donjon in pieces[3][0]:
        if donjon.position[0] == caseclic[1] and donjon.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1),(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1)],[(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2),(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2)],[(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3),(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.sousniveau!=piecebase.sousniveau:
                                            caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                i += 1   

            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case01,case11):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
            caseCanMove[0]=[]

            caseok=caseCanMove
            caseCanMove=[]
            for surcase in caseok:
                for case in surcase:
                    caseCanMove.append(case)
        
    for empereur in pieces[4][0]:
        if empereur.position[0] == caseclic[1] and empereur.position[1] == caseclic[0]:
            caseCanMove = [[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1)],[(caseclic[0],caseclic[1]+2),(caseclic[0]+2,caseclic[1]+2),(caseclic[0]+2,caseclic[1]),
                (caseclic[0]+2,caseclic[1]-2),(caseclic[0],caseclic[1]-2),(caseclic[0]-2,caseclic[1]-2),(caseclic[0]-2,caseclic[1]),
                (caseclic[0]-2,caseclic[1]+2)],[(caseclic[0],caseclic[1]+3),(caseclic[0]+3,caseclic[1]+3),(caseclic[0]+3,caseclic[1]),
                (caseclic[0]+3,caseclic[1]-3),(caseclic[0],caseclic[1]-3),(caseclic[0]-3,caseclic[1]-3),(caseclic[0]-3,caseclic[1]),
                (caseclic[0]-3,caseclic[1]+3)],[(caseclic[0],caseclic[1]+4),(caseclic[0]+4,caseclic[1]+4),(caseclic[0]+4,caseclic[1]),
                (caseclic[0]+4,caseclic[1]-4),(caseclic[0],caseclic[1]-4),(caseclic[0]-4,caseclic[1]-4),(caseclic[0]-4,caseclic[1]),
                (caseclic[0]-4,caseclic[1]+4)]]
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[0][c-i]
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[1][c-i]
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1

            i=0
            caseAsupprimer=[]
            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1                      
            i=0
            for c in caseAsupprimer:
                del caseCanMove[2][c-i]
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                     caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            i=0
            caseAsupprimer=[]
            for case in caseCanMove[3]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.niveau == piecebase.niveau:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                else:
                                    if piece.couleur==piecebase.couleur:
                                        caseAsupprimer.append(i)
                                    else:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=joueur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    caseAsupprimer.append(i)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[3][c-i]
                i += 1
            
            for case in caseCanMove[0]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case01=caseclic[0]
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+1:
                                        case01=caseclic[0]+2
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-1:
                                        case01=caseclic[0]-2
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case11=caseclic[1]
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+1:
                                        case11=caseclic[1]+2
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-1:
                                        case11=caseclic[1]-2
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[1]:
                                        if casesupp==(case01,case11):
                                            caseCanMove[1].remove(casesupp)
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case01=caseclic[0]
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+1:
                                            case01=caseclic[0]+2
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-1:
                                            case01=caseclic[0]-2
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case11=caseclic[1]
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+1:
                                            case11=caseclic[1]+2
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-1:
                                            case11=caseclic[1]-2
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[1]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[1].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[1]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case02=caseclic[0]
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+2:
                                        case02=caseclic[0]+3
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-2:
                                        case02=caseclic[0]-3
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case12=caseclic[1]
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+2:
                                        case12=caseclic[1]+3
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-2:
                                        case12=caseclic[1]-3
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[2]:
                                        if casesupp==(case02,case12):
                                            caseCanMove[2].remove(casesupp)
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case02=caseclic[0]
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+2:
                                            case02=caseclic[0]+3
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-2:
                                            case02=caseclic[0]-3
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case12=caseclic[1]
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+2:
                                            case12=caseclic[1]+3
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-2:
                                            case12=caseclic[1]-3
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case02,case12):
                                                caseCanMove[2].remove(casesupp)
                                        for casesupp in caseCanMove[2]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)

            for case in caseCanMove[2]:
                br=False
                for niveau in pieces:
                    if br==True:
                        break
                    for sousniveau in niveau:
                        if br==True:
                            break
                        for piece in sousniveau:
                            if br==True:
                                break
                            if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                br=True
                                if piece.couleur!=piecebase.couleur:
                                    if case[0]==caseclic[0]:
                                        case03=caseclic[0]
                                    elif case[0]==caseclic[0]+3:
                                        case03=caseclic[0]+4
                                    elif case[0]==caseclic[0]-3:
                                        case03=caseclic[0]-4
                                    if case[1]==caseclic[1]:
                                        case13=caseclic[1]
                                    elif case[1]==caseclic[1]+3:
                                        case13=caseclic[1]+4
                                    elif case[1]==caseclic[1]-3:
                                        case13=caseclic[1]-4
                                    for casesupp in caseCanMove[3]:
                                        if casesupp==(case03,case13):
                                            caseCanMove[3].remove(casesupp)
                                if piece.couleur==piecebase.couleur:
                                    if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                        if case[0]==caseclic[0]:
                                            case03=caseclic[0]
                                        elif case[0]==caseclic[0]+3:
                                            case03=caseclic[0]+4
                                        elif case[0]==caseclic[0]-3:
                                            case03=caseclic[0]-4
                                        if case[1]==caseclic[1]:
                                            case13=caseclic[1]
                                        elif case[1]==caseclic[1]+3:
                                            case13=caseclic[1]+4
                                        elif case[1]==caseclic[1]-3:
                                            case13=caseclic[1]-4
                                        for casesupp in caseCanMove[3]:
                                            if casesupp==(case03,case13):
                                                caseCanMove[3].remove(casesupp)
            caseok=caseCanMove
            caseCanMove=[]
            for surcase in caseok:
                for case in surcase:
                    caseCanMove.append(case)

    for jumper in pieces[2][1]:
        if jumper.position[0] == caseclic[1] and jumper.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+1)],[(caseclic[0],caseclic[1]+2)],
                            [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1)]],
                            [[(caseclic[0]+1,caseclic[1])],[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),
                                (caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]-1)]],
                            [[(caseclic[0],caseclic[1]-1)],[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),
                                (caseclic[0]+1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1)]],
                            [[(caseclic[0]-1,caseclic[1])],[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),
                                (caseclic[0]-1,caseclic[1]+1),(caseclic[0]-1,caseclic[1]-1)]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[2]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau!=piecebase.sousniveau:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[2][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[1][0]
                sautcase=types[0][0]
                if len(types[2])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True

                if sup==False:
                    saut=False
                    for niveau in pieces:
                        if saut==False:
                            for sousniveau in niveau:
                                if saut==False:
                                    for piece in sousniveau:
                                        if saut==False:
                                            if piece.position[1]==sautcase[0] and piece.position[0]==sautcase[1]:
                                                saut=True
                    if saut==False:
                        typesAsupprimer.append(i)
                        sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[]
            for type in casesprov:
                for case in type[2]:
                    caseCanMove.append(case)

    for chapelain in pieces[3][1]:
        if chapelain.position[0] == caseclic[1] and chapelain.position[1] == caseclic[0]:
            caseCanMove = [[[(caseclic[0],caseclic[1]+2)],
                           [(caseclic[0]-1,caseclic[1]+3),(caseclic[0]+1,caseclic[1]+3),(caseclic[0],caseclic[1]+1)]],
                            [[(caseclic[0]+2,caseclic[1])],
                            [(caseclic[0]+3,caseclic[1]+1),(caseclic[0]+3,caseclic[1]-1),(caseclic[0]+1,caseclic[1])]],
                            [[(caseclic[0],caseclic[1]-2)],
                            [(caseclic[0]+1,caseclic[1]-3),(caseclic[0]-1,caseclic[1]-3),(caseclic[0],caseclic[1]-1)]],
                            [[(caseclic[0]-2,caseclic[1])],
                            [(caseclic[0]-3,caseclic[1]+1),(caseclic[0]-3,caseclic[1]-1),(caseclic[0]-1,caseclic[1])]]]
    
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(c)
                    c += 1
                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1
            
            for typecase in caseCanMove:
                caseAsupprimer=[]
                tous=typecase[1]
                c=0
                for case in tous:
                    for niveaux in pieces:
                        for sousniveaux in niveaux:
                            for piece in sousniveaux:
                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                    if piece.couleur==piecebase.couleur:
                                        if piece.niveau==piecebase.niveau:
                                            if piece.sousniveau!=piecebase.sousniveau:
                                                caseAsupprimer.append(c)
                                        else:
                                            caseAsupprimer.append(c)
                                    else:
                                        if piece.niveau==0:
                                            caseAsupprimer.append(c)

                    c += 1

                i=0
                for c in caseAsupprimer:
                    del typecase[1][c-i]
                    i += 1

            typesAsupprimer = []
            i=0
            for types in caseCanMove:
                sup=False
                semicase=types[0][0]
                if len(types[1])==0:
                    typesAsupprimer.append(i)
                    sup=True
                if sup==False:
                    if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                        typesAsupprimer.append(i)
                        sup=True
                    else:
                        semi=False
                        for niveau in pieces:
                            if semi==False:
                                for sousniveau in niveau:
                                    if semi==False:
                                        for piece in sousniveau:
                                            if semi==False:
                                                if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                    semi=True
                        if semi==True:
                            typesAsupprimer.append(i)
                            sup=True
                i += 1
            i=0
            for t in typesAsupprimer:
                del caseCanMove[t-i]
                i += 1

            
            casesprov=caseCanMove
            caseCanMove=[]
            for type in casesprov:
                for case in type[1]:
                    caseCanMove.append(case)

    if Grab[joueur-1][1]==piecebase:
        ok=True
    return caseCanMove,ok