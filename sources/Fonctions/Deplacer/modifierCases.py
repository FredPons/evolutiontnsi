"""
    ces fonctions modifient les cases selon les pieces qu'il ya à déplcer et selon le grab
"""

def modifierCases(cases,ancpos,newpos,pieceadeplacer):
    cases[(ancpos[0])-1][(ancpos[1])-1]=(False,0)
    cases[(newpos[0])-1][(newpos[1])-1]=(True,pieceadeplacer.couleur)
    return cases

def modifierCases2(cases,newpos,pieceadeplacer):
    cases[(newpos[1])-1][(newpos[0])-1]=(True,pieceadeplacer.couleur)
    return cases

def modifierCasesGrab(cases,ancpos,newpos,ancposgardien,joueur):
    cases[(ancpos[0])-1][(ancpos[1])-1]=(False,0)
    cases[(newpos[0])-1][(newpos[1])-1]=(True,joueur)
    cases[(ancposgardien[0])-1][(ancposgardien[1])-1]=(False,0)
    cases[(ancpos[0])-1][(ancpos[1])-1]=(True,joueur)
    return cases


