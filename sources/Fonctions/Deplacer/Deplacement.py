from Fonctions.Deplacer.caseCanMove import *
from _Initialisation_.Creer.creerPieces import *
from Fonctions.GererEchec.echecOuPas import *
from Fonctions.Deplacer.modifierCases import *
from random import randint

def quelleCaseClic(x,y,joueur,choixremettre):
    """détermine sur quelle case on a cliqué

    Args:
        x (int): abscisse
        y (int): ordonée
        joueur (int): numero du joueur
        choixremettre (list): choix du refresh


    """
    if joueur==1:
        xp=560
        yp=140
        clic=False
        caseclic=(0,0)
        if x>xp and x<=xp+800 and y>yp and y<=yp+800:
            for i in range (10):
                xl=i+1
                if x>xp and x<=(xp+80) and clic==False:
                    for i in range (10):
                        yc=10-i
                        if y>yp and y<=(yp+80) and clic==False:
                            caseclic=(xl,yc)
                        yp += 80
                xp += 80
        elif x>410 and x<530:
            if y<940 and y>820:
                caseclic=(1,"A")
            elif y<820 and y>700:
                caseclic=(1,"B")
            elif y<700 and y>580:
                caseclic=(1,"C")
            elif y<580 and y>460:
                caseclic=(1,"D")
            elif y<460 and y>340:
                caseclic=(1,"E")
        elif x>320 and x<400:
            if choixremettre[0]==True:
                n=choixremettre[1][1]
                k=320
                k += (120*(n-1))
                if y>(k+80) and y<(k+160):
                    caseclic=[1,n,"p"]
                elif y>k and y<(k+80):
                    caseclic=[1,n,"r"]
            
    if joueur==2:
        xp=560
        yp=140
        clic=False
        caseclic=(0,0)
        if x>xp and x<=xp+800 and y>yp and y<=yp+800:
            for i in range (10):
                xl=10-i
                if x>xp and x<=(xp+80) and clic==False:
                    for i in range (10):
                        yc=i+1
                        if y>yp and y<=(yp+80) and clic==False:
                            caseclic=(xl,yc)
                            clic=True
                        yp += 80
                xp += 80
        elif x>410 and x<530:
            if y<940 and y>820:
                caseclic=(2,"A")
            elif y<820 and y>700:
                caseclic=(2,"B")
            elif y<700 and y>580:
                caseclic=(2,"C")
            elif y<580 and y>460:
                caseclic=(2,"D")
            elif y<460 and y>340:
                caseclic=(2,"E")
        elif x>320 and x<400:
            if choixremettre[0]==True:
                n=choixremettre[1][1]
                k=320
                k += (120*(n-1))
                if y>(k+80) and y<(k+160):
                    caseclic=[2,n,"p"]
                elif y>k and y<(k+80):
                    caseclic=[2,n,"r"]

    return caseclic

def pieceSurCaseEtCouleur(caseclic,cases,joueur):
    """détermine si cette caseclic est monopolisée par une de nos piece

    """
    if caseclic!=(0,0) and type(caseclic[1])!=str:
        colone=caseclic[0]
        ligne=caseclic[1]
        joueurCase=cases[ligne-1][colone-1][1]
        if joueurCase==joueur:
            possibiliteDeBouger=True
        else:
            possibiliteDeBouger=False  
    else:
        possibiliteDeBouger=False
    return possibiliteDeBouger

def bougerall(x,y,joueur,cases,boitePieces,caseGrab,Grab,banc,choixremettre):
    """
    fonction qui gère tout après le premier clic
    """
    pieces=boitePieces.pieces
    caseCanMove=[]
    piecebase=None
    pieceGrab=None
    Canfusion=[0,0]
    possibiliteDeBouger=False
    caseclic=quelleCaseClic(x,y,joueur,choixremettre)
    possibiliteDeBouger=pieceSurCaseEtCouleur(caseclic,cases,joueur)
    if possibiliteDeBouger==True:
        if Grab[joueur-1][0]==False:
            caseCanMove,Canfusion,piecebase,caseGrab,pieceGrab=casesPieceCanMove(caseclic,pieces,joueur)
            caseCanMove=changercasessiGrab(False,piecebase,joueur,boitePieces,caseCanMove,cases,pieces,Grab)
        else:
            caseCanMove,Canfusion,piecebase,caseGrab,pieceGrab,ok=casesPieceCanMoveGrab(caseclic,pieces,joueur,Grab)
            caseCanMove=changercasessiGrab(ok,piecebase,joueur,boitePieces,caseCanMove,cases,pieces,Grab)
    else:
        if type(caseclic[1])==str:
            l=caseclic[1]
            if l=="A":
                n=5
            elif l=="B":
                n=4
            elif l=="C":
                n=3
            elif l=="D":
                n=2
            elif l=="E":
                n=1
            if len(banc.piecesshop)>=n:
                choixremettre=(True,(joueur,n))


    return caseCanMove,possibiliteDeBouger,caseclic,Canfusion,piecebase,caseGrab,pieceGrab,pieces,choixremettre

def changerclics(Echec,clic,possibiliteDeBouger,caseCanMove,x,y,joueur,pieces,cases,
                 
                 caseclic,boitePieces,Canfusion,piecebase,images_jeu,fenetrejeu,caseGrab,pieceGrab,Grab,banc,
                 choixremettre,canPlay):
    """
    fonction qui gère tout après le deuxieme clic
    """
    SemiGrab=False
    if clic==True:
        caseclic2=quelleCaseClic(x,y,joueur,choixremettre)
        if choixremettre[0]!=True:
            peutyaller=CanGo(caseclic2,caseCanMove,piecebase)
            SemiGrab=Grabfunction(caseclic2,caseGrab)
            if peutyaller==True:
                pieces,cases,Echec,canPlay=changerall(pieces,cases,caseclic,caseclic2,joueur,boitePieces,Canfusion,images_jeu,
                                                fenetrejeu,Grab,banc,canPlay,Grab[joueur-1])
            clic=False
            if SemiGrab==True:
                if Grab[joueur-1][0]==True:
                    Grab[joueur-1]=(False,None)
                elif Grab[joueur-1][0]==False:
                    Grab[joueur-1]=(True,pieceGrab)
                clic=False
                return clic,pieces,cases,Echec,caseGrab,Grab,canPlay
            return clic,pieces,cases,Echec,[],Grab,canPlay
        else:
            casesRefresh=[]
            refresh=False
            if len(caseclic2)>2:
                if caseclic2[2]=="p":
                    p = 1
                    for piece in banc.piecesshop:
                        if piece.couleur!=caseclic2[0]:
                            if p == caseclic2[1]:
                                banc.remove_piece(piece,banc.piecesshop)
                            p += 1
                elif caseclic2[2]=="r":
                    if Echec[joueur-1][0]==False:
                        casesRefresh=casesCanRefresh(pieces,joueur,cases)
                        refresh=True
            return banc,refresh,casesRefresh
    elif (clic==False and (possibiliteDeBouger==True or type(caseclic[1])==str)):
        clic=True
    return clic,Echec

def changerclics2(banc,x,y,joueur,choixremettre,casesRefresh,boitePieces,clic,cases,canPlay):
    """
    fonction qui gère tout après le deuxieme clic lorqu'il y a un refresh
    """
    if clic==True:
        caseclic2=quelleCaseClic(x,y,joueur,choixremettre)
        peutyaller=CanGo2(caseclic2,casesRefresh)
        if peutyaller==True:
            banc,boitePieces.pieces,cases=changerall2(choixremettre,joueur,caseclic2,banc,boitePieces,cases)
            canPlay=False
            clic=False
    return banc,clic,boitePieces.pieces,cases,canPlay

def CanGo(caseclic2,caseCanMove,piecebase):
    """
    piece base peut -elle aller sur caseclic2

    
    """
    if piecebase == None or piecebase.niveau==1:
        for case in caseCanMove:
            if case==caseclic2:
                return True
    else:
        if piecebase.niveau==2 or piecebase.niveau==3 or piecebase.niveau==4:
            if piecebase.sousniveau==0:
                for case in caseCanMove:
                    for souscase in case:
                        if souscase==caseclic2:
                            return True
            elif piecebase.sousniveau==1:
                for types in caseCanMove[1]:
                    for case in types:
                        if case==caseclic2:
                            return True
    return False

def CanGo2(caseclic2,casesRefresh):
    """
    caseclic2 est elle une case de refresh disponible
    
    """
    for case in casesRefresh:
        if case==caseclic2:
            return True
    return False

def Grabfunction(caseclic2,caseGrab):
    """
    esce que on veut grab ou dégrab
    
    """
    if caseGrab!=None:
        for case in caseGrab:
            if case==caseclic2:
               return True
    return False

def changerall(pieces,cases,caseclic,caseclic2,joueur,boitePiece,Canfusion,images_jeu,fenetrejeu,Grab,banc,canPlay,Grabjoueur):
    """
    change tous les parametres après le deuxieme clic, (fusion, manger, déplacement)
    
    """
    for niveau in pieces:
        for sousniveau in niveau:
            for piece in sousniveau:
                if piece.position[0]==caseclic[1] and piece.position[1]==caseclic[0]:
                    piecebase=piece
    manger=False
    fusion=False
    if joueur==1:
        if cases[caseclic2[1]-1][caseclic2[0]-1][1]==2 :
            manger=True
    elif joueur==2:
        if cases[caseclic2[1]-1][caseclic2[0]-1][1]==1 :
            manger=True
    newpos=(caseclic2[1],caseclic2[0])
    ancpos=(caseclic[1],caseclic[0])
    for i in range(4):
        for sousniveau in pieces[i+1]:
            for piece in sousniveau:
                if piece.couleur!=piecebase.couleur:
                    if piece.position[0]==caseclic2[1] and piece.position[1]==caseclic2[0]:
                        pieceamanger=piece
    for i in range(4):
        for sousniveau in pieces[i+1]:
            for pieceadeplacer in sousniveau:
                if pieceadeplacer.position[0]==caseclic[1] and pieceadeplacer.position[1]==caseclic[0]:
                    if Canfusion[0]>0:
                        for k in range(4):      
                            for sousniveau in pieces[k+1]:
                                for piece in sousniveau:
                                    if piece.position[0]==caseclic2[1] and piece.position[1]==caseclic2[0]:
                                        if piecebase.couleur==piece.couleur:
                                            if piece.niveau==piecebase.niveau:
                                                pieceafusionner=piece
                                                fusion=True
                                                
    if manger==True:
        ok=False
        pieces=boitePiece.remove_piece(pieceamanger,pieces)
        c1 = 0
        c2 = 0
        for piece in banc.piecesshop:
            if piece.couleur==1:
                c1 +=1
            elif piece.couleur==2:
                c2 += 1
        if pieceamanger.couleur==1:
            if c1<5:
                banc.add_piece(pieceamanger,banc.piecesshop)
        elif pieceamanger.couleur==2:
            if c2<5:
                banc.add_piece(pieceamanger,banc.piecesshop)
        piecebase.deplacer(newpos)
        if Grab[joueur-1][1]==piecebase:
            for gardien in pieces[0][0]:
                if gardien.couleur==piecebase.couleur:
                    ancposgardien=gardien.position
                    gardien.deplacer(ancpos)
                    ok=True
                    cases=modifierCasesGrab(cases,ancpos,newpos,ancposgardien,joueur)
        if ok==False:
            cases=modifierCases(cases,ancpos,newpos,piecebase)
        canPlay=False
    if fusion==False and manger==False:
        ok=False
        piecebase.deplacer(newpos)
        if Grab[joueur-1][1]==piecebase:
            for gardien in pieces[0][0]:
                if gardien.couleur==piecebase.couleur:
                    ancposgardien=gardien.position
                    gardien.deplacer(ancpos)
                    cases=modifierCasesGrab(cases,ancpos,newpos,ancposgardien,joueur)
                    ok=True
        if ok==False:
            cases=modifierCases(cases,ancpos,newpos,piecebase)
        canPlay=False
    elif fusion==True:
        for i in range(4):
            for sousniveau in pieces[i+1]:
                for piece in sousniveau:
                    if piece.position[0]==caseclic[1] and piece.position[1]==caseclic[0]:
                        pieceasupprimer=piece
        pieces,cases,canPlay=creerPiecesFusion(caseclic2,boitePiece,joueur,Canfusion,pieces,pieceafusionner,pieceasupprimer,
                                       ancpos,newpos,cases,images_jeu,fenetrejeu,canPlay)
    
    Echec=echecOuPas(pieces,joueur)
    return pieces,cases,Echec,canPlay

def changerall2(choixremettre,joueur,caseclic,banc,boitePieces,cases):
    """
    change tous les parametres après le deuxieme clic, (refresh)
    
    """
    joueur=choixremettre[1][0]
    n=choixremettre[1][1]
    j=0
    for pieceshop in banc.piecesshop:
        if pieceshop.couleur!=joueur:
            j=j+1
            if j==n:
                if pieceshop.niveau==1:
                    pieceok=pieceshop
                    pieceok.position=(caseclic[1],caseclic[0])
                    pieceok.couleur=joueur
                elif pieceshop.niveau==2:
                    pieceok=Soldat(joueur,(caseclic[1],caseclic[0]))
                else :
                    sousniv=randint(1,2)
                    if pieceshop.niveau==3:
                        if sousniv==1:
                            pieceok=Bastion(joueur,(caseclic[1],caseclic[0]))
                        else:
                            pieceok=Jumper(joueur,(caseclic[1],caseclic[0]))
                    elif pieceshop.niveau==4:
                        if sousniv==1:
                            pieceok=Donjon(joueur,(caseclic[1],caseclic[0]))
                        else:
                            pieceok=Chapelain(joueur,(caseclic[1],caseclic[0]))
                banc.remove_piece(pieceshop,banc.piecesshop)
                boitePieces.add_piece(pieceok,boitePieces.pieces)
                cases=modifierCases2(cases,caseclic,pieceok)


    return banc,boitePieces.pieces,cases