from Fonctions.GererEchec.regarderFin import echecEtMath,Ppat,defMat,patF
from Fonctions.GererEchec.echecOuPas import checkMat
def apresCoup(Echec,joueur,
        cases,boitePieces,client_socket,banc,pieces,run,wait,canPlay,choixremettre,piecebase,caseCanMove,casesRefresh,caseGrab,clic,check):

    """
    Cette fonction regarde vérifie si il y a une fin de partie et renvoie les informations en conséquences
    """
    choixremettre=(False,(0,0))
    piecebase=None
    caseCanMove=[]
    casesRefresh=[]
    caseGrab=[]
    clic=False
    if Echec[joueur-1][0]==True:
        mat=checkMat(joueur,cases,boitePieces)
        if mat != 0:
            echecEtMath(client_socket,mat)
            print('Vous avez perdu par Echec et Mat')
            wait=False
            canPlay=False
            run=False
        else:
            check+=1
            if check==15:
                Ppat(client_socket)
                print('Vous avez fais égalité par échec perpétuel(15)')
                wait=False
                canPlay=False
                run=False

    else :
        check=0
        mat=checkMat(joueur,cases,boitePieces)
        if mat != 0:
            ok=False
            for piece in banc.piecesshop:
                if piece.couleur!=joueur:
                    ok=True
            if ok==False:
                run=False
                wait=False
                canPlay=False
                print('Vous avez perdu par manque de matériel')
                defMat(client_socket)
            else:
                ok=False
                for gardien in pieces[0][0]:
                    if gardien.couleur==joueur:
                        caseclic=(gardien.position[1],gardien.position[0])
                casestour=[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                (caseclic[0]-1,caseclic[1]+1)]
                i=0
                caseAsupprimer=[]
                for case in casestour:
                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                        caseAsupprimer.append(i)
                    i += 1
                i=0
                for c in caseAsupprimer:
                    del casestour[c-i]
                    i += 1
                for case in casestour:
                    if cases[case[1]-1][case[0]-1][0]==False:
                        ok=True
                if ok==False:
                    run=False
                    wait=False
                    canPlay=False
                    print('Vous avez perdu par manque de matériel')
                    defMat(client_socket)
        elif mat==0 or ok==True:
            ok=False
            if len(pieces[1][0])==0 and len(pieces[2][0])==0 and len(pieces[3][0])==0 and len(pieces[3][1])==0 and len(pieces[4][0])==0:
                if len(banc.piecesshop)==0:
                    print('Vous avez fait égalité par double manque de pieces majeures')
                    patF(client_socket)
                else:
                    ok=False
                    ok2=False
                    for gardien in pieces[0][0]:
                        if ok2!=True:
                            caseclic=(gardien.position[1],gardien.position[0])
                            casestour=[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                            (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                            (caseclic[0]-1,caseclic[1]+1)]
                            i=0
                            caseAsupprimer=[]
                            for case in casestour:
                                if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                    caseAsupprimer.append(i)
                                i += 1
                            i=0
                            for c in caseAsupprimer:
                                del casestour[c-i]
                                i += 1
                            for case in casestour:
                                if cases[case[1]-1][case[0]-1][0]==False:
                                    ok=True
                            if ok==False:
                                ok2=False
                                run=False
                                wait=False
                                canPlay=False
                                print('Vous avez fait égalité par double manque de pieces majeures')
                                patF(client_socket)
    return run,wait,canPlay,choixremettre,piecebase,caseCanMove,casesRefresh,caseGrab,clic,check