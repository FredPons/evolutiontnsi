import pygame
def afficherplateau(fenetrejeu,images_jeu,joueur,banc):
    """
    affichage du plateau, du banc et des pieces du banc

    Args:
        fenetrejeu (objet): fenetre pygame
        images_jeu (list): liste de images
        joueur (int): numero associé au joueur
        banc (objet): banc contenant les pieces
    """
    policePlateau = pygame.font.Font("police_plateau_.ttf", 30)

    image_rect = images_jeu[0].get_rect(center=(1920/2, 1080/2))
    x=560
    y=940
    fenetrejeu.blit(images_jeu[0], image_rect)
    fenetrejeu.blit(images_jeu[13], (x-150,y-620))
    fenetrejeu.blit(images_jeu[13], (1920-530,150))
    
    if joueur==1:
        nomcases=[['1','2','3','4','5','6','7','8','9','10'],['a','b','c','d','e','f','g','h','i','j']]
        for i in nomcases[0]:
            y -= 80
            texteChiffre = i
            chiffre = policePlateau.render(texteChiffre,True, (255,255,0))
            position=(x+3,y-4)
            fenetrejeu.blit(chiffre,position)
        for i in nomcases[1]:
            y=940
            x += 80
            texteLettre = i
            lettre = policePlateau.render(texteLettre,True, (255,255,0))
            position=(x-12,y-35)
            fenetrejeu.blit(lettre,position)

    elif joueur==2:
        nomcases=[['10','9','8','7','6','5','4','3','2','1'],['j','i','h','g','f','e','d','c','b','a']]
        for i in nomcases[0]:
            y -= 80
            texteChiffre = i
            chiffre = policePlateau.render(texteChiffre,True, (255,255,0))
            position=(x+3,y-4)
            fenetrejeu.blit(chiffre,position)
        for i in nomcases[1]:
            y=940
            x += 80
            texteLettre = i
            lettre = policePlateau.render(texteLettre,True, (255,255,0))
            position=(x-12,y-35)
            fenetrejeu.blit(lettre,position)
    
    xn = 1380
    yn = 202.5
    xy = 530
    yy = 372.5
    for piecesshop in banc.piecesshop:
        if piecesshop.couleur==1:
            piecesshop.position=(0,0)
            if joueur==2:
                if piecesshop.niveau==1:
                    fenetrejeu.blit(images_jeu[2][0],(xy-82.5,yy))
                    yy += 127.5
                elif piecesshop.niveau==2:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[3][0],(xy-82.5,yy))
                        yy += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[4][0],(xy-82.5,yy))
                        yy += 127.5
                elif piecesshop.niveau==3:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[5][0],(xy-82.5,yy))
                        yy += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[6][0],(xy-82.5,yy))
                        yy += 127.5
                elif piecesshop.niveau==4:
                    fenetrejeu.blit(images_jeu[7][0],(xy-82.5,yy))
                    yy += 127.5
            if joueur==1:
                if piecesshop.niveau==1:
                    fenetrejeu.blit(images_jeu[2][0],(xn+47.5,yn))
                    yn += 127.5
                elif piecesshop.niveau==2:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[3][0],(xn+47.5,yn))
                        yn += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[4][0],(xn+47.5,yn))
                        yn += 127.5
                elif piecesshop.niveau==3:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[5][0],(xn+47.5,yn))
                        yn += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[6][0],(xn+47.5,yn))
                        yn += 127.5
                elif piecesshop.niveau==4:
                    fenetrejeu.blit(images_jeu[7][0],(xn+47.5,yn))
                    yn += 127.5
        elif piecesshop.couleur==2:
            piecesshop.position=(0,0)
            if joueur==1:
                if piecesshop.niveau==1:
                    fenetrejeu.blit(images_jeu[2][1],(xy-82.5,yy))
                    yy += 127.5
                elif piecesshop.niveau==2:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[3][1],(xy-82.5,yy))
                        yy += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[4][1],(xy-82.5,yy))
                        yy += 127.5
                elif piecesshop.niveau==3:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[5][1],(xy-82.5,yy))
                        yy += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[6][1],(xy-82.5,yy))
                        yy += 127.5
                elif piecesshop.niveau==4:
                    fenetrejeu.blit(images_jeu[7][1],(xy-82.5,yy))
                    yy += 127.5
            if joueur==2:
                if piecesshop.niveau==1:
                    fenetrejeu.blit(images_jeu[2][1],(xn+47.5,yn))
                    yn += 127.5
                elif piecesshop.niveau==2:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[3][1],(xn+47.5,yn))
                        yn += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[4][1],(xn+47.5,yn))
                        yn += 127.5
                elif piecesshop.niveau==3:
                    if piecesshop.sousniveau==0:
                        fenetrejeu.blit(images_jeu[5][1],(xn+47.5,yn))
                        yn += 127.5
                    if piecesshop.sousniveau==1:
                        fenetrejeu.blit(images_jeu[6][1],(xn+47.5,yn))
                        yn += 127.5
                elif piecesshop.niveau==4:
                    fenetrejeu.blit(images_jeu[7][1],(xn+47.5,yn))
                    yn += 127.5
        
def afficherpieces(fenetrejeu,images_jeu,pieces,joueur):
    """
    affichage des pieces

    Args:
        fenetrejeu (objet): fenetre pygame
        images_jeu (list): liste de images
        joueur (int): numero associé au joueur
        pieces (list): liste contenant les pieces
    """
    gardiens=pieces[0][0]
    soldats=pieces[1][0]
    bastions=pieces[2][0]
    jumpers=pieces[2][1]
    donjons=pieces[3][0]
    chapelains=pieces[3][1]
    empereurs=pieces[4][0]
    if joueur==1:
        for i in gardiens:
            positiongardien=i.position
            posafficher=(((560+(positiongardien[1]-1)*80)+10),((940-positiongardien[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[1][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[1][0],posafficher)
        for i in soldats:
            positionsoldat=i.position
            posafficher=(((560+(positionsoldat[1]-1)*80)+10),((940-positionsoldat[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[2][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[2][0],posafficher)
        for i in bastions:
            positionbastion=i.position
            posafficher=(((560+(positionbastion[1]-1)*80)+10),((940-positionbastion[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[3][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[3][0],posafficher)
        for i in jumpers:
            positionjumper=i.position
            posafficher=(((560+(positionjumper[1]-1)*80)+10),((940-positionjumper[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[4][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[4][0],posafficher)
        for i in donjons:
            positiondonjon=i.position
            posafficher=(((560+(positiondonjon[1]-1)*80)+10),((940-positiondonjon[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[5][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[5][0],posafficher)
        for i in chapelains:
            positionchapelain=i.position
            posafficher=(((560+(positionchapelain[1]-1)*80)+10),((940-positionchapelain[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[6][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[6][0],posafficher)
        for i in empereurs:
            positionempereur=i.position
            posafficher=(((560+(positionempereur[1]-1)*80)+10),((940-positionempereur[0]*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[7][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[7][0],posafficher)
    elif joueur==2:
        for i in gardiens:
            positiongardien=i.position
            posafficher=(((1360-(positiongardien[1])*80)+10),((140+(positiongardien[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[1][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[1][0],posafficher)
        for i in soldats:
            positionsoldat=i.position
            posafficher=(((1360-(positionsoldat[1])*80)+10),((140+(positionsoldat[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[2][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[2][0],posafficher)
        for i in bastions:
            positionbastion=i.position
            posafficher=(((1360-(positionbastion[1])*80)+10),((140+(positionbastion[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[3][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[3][0],posafficher)
        for i in jumpers:
            positionjumper=i.position
            posafficher=(((1360-(positionjumper[1])*80)+10),((140+(positionjumper[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[4][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[4][0],posafficher)
        for i in donjons:
            positiondonjon=i.position
            posafficher=(((1360-(positiondonjon[1])*80)+10),((140+(positiondonjon[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[5][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[5][0],posafficher)
        for i in chapelains:
            positionchapelain=i.position
            posafficher=(((1360-(positionchapelain[1])*80)+10),((140+(positionchapelain[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[6][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[6][0],posafficher)
        for i in empereurs:
            positionempereur=i.position
            posafficher=(((1360-(positionempereur[1])*80)+10),((140+(positionempereur[0]-1)*80)+10))
            if i.couleur==1:
                fenetrejeu.blit(images_jeu[7][1],posafficher)
            else:
                fenetrejeu.blit(images_jeu[7][0],posafficher)

def afficherpts(fenetrejeu,images_jeu,joueur,caseCanMove,clic,piecebase,casesRefresh):
    """
    affiche les points de deplacement, de pivot, et de refresh

    Args:
        fenetrejeu (objet): fenetre pygame
        images_jeu (list): liste de images
        joueur (int): numero associé au joueur
        caseCanMove (list): liste de cases de deplacementent de pivot
        clic (bool): si un clic à été effectué
        piecebase (object): piece sur laquelle on a cliqué
        casesRefresh (list): liste des cases de refresh
    """
    if clic==True:
        i=0
        for case in caseCanMove:
            if joueur==1:
                if piecebase == None or piecebase.niveau==1:
                    posafficher=(((560+(case[0]-1)*80)+10),((940-(case[1])*80)+10))
                    fenetrejeu.blit(images_jeu[8],posafficher)
                else:
                    if piecebase.niveau==2 or piecebase.niveau==3 or piecebase.niveau==4:
                        if piecebase.sousniveau==0:
                            for souscase in case:
                                posafficher=(((560+(souscase[0]-1)*80)+10),((940-(souscase[1])*80)+10))
                                fenetrejeu.blit(images_jeu[8],posafficher)
                        elif piecebase.sousniveau==1:
                            if i==1:
                                for finales in case:
                                    for casefinale in finales:
                                        posafficher=(((560+(casefinale[0]-1)*80)+10),((940-(casefinale[1])*80)+10))
                                        fenetrejeu.blit(images_jeu[8],posafficher)
                            else:
                                for casefinale in case:
                                    posafficher=(((560+(casefinale[0]-1)*80)+10),((940-(casefinale[1])*80)+10))
                                    fenetrejeu.blit(images_jeu[9],posafficher)

            if joueur==2:
                if piecebase == None or piecebase.niveau==1:
                    posafficher=(((1360-case[0]*80)+10),((140+(case[1]-1)*80)+10))
                    fenetrejeu.blit(images_jeu[8],posafficher)
                else:
                    if piecebase.niveau==2 or piecebase.niveau==3 or piecebase.niveau==4:
                        if piecebase.sousniveau==0:
                            for souscase in case:
                                posafficher=(((1360-souscase[0]*80)+10),((140+(souscase[1]-1)*80)+10))
                                fenetrejeu.blit(images_jeu[8],posafficher)
                        elif piecebase.sousniveau==1:
                            if i==1:
                                for finales in case:
                                    for casefinale in finales:
                                        posafficher=(((1360-casefinale[0]*80)+10),((140+(casefinale[1]-1)*80)+10))
                                        fenetrejeu.blit(images_jeu[8],posafficher)
                            else:
                                for casefinale in case:
                                    posafficher=(((1360-casefinale[0]*80)+10),((140+(casefinale[1]-1)*80)+10))
                                    fenetrejeu.blit(images_jeu[9],posafficher)
            i +=1
        
        if len(caseCanMove)==0:
            for case in casesRefresh:
                if joueur==1:
                    posafficher=(((560+(case[0]-1)*80)+10),((940-(case[1])*80)+10))
                    fenetrejeu.blit(images_jeu[17],posafficher)
                elif joueur==2:
                    posafficher=(((1360-case[0]*80)+10),((140+(case[1]-1)*80)+10))
                    fenetrejeu.blit(images_jeu[17],posafficher)
    
def afficherdanger(fenetrejeu,images_jeu,pieces,joueur,Echec):
    """
    affiche le danger
    """
    for i in range (2):
        if Echec[i][0]==True:
            endanger=Echec[i][1]
            for gardien in pieces[0][0]:
                if gardien.couleur==endanger:
                    positiongardien=gardien.position
                    if joueur==1:
                        posafficher=(((560+(positiongardien[1]-1)*80)+10),((940-positiongardien[0]*80)+10))
                    elif joueur==2:
                        posafficher=(((1360-(positiongardien[1])*80)+10),((140+(positiongardien[0]-1)*80)+10))
                    fenetrejeu.blit(images_jeu[11],posafficher)

def affichergrab(fenetrejeu,images_jeu,joueur,caseGrab,Grab,pieces):
    """
    affiche le grab
    """
    if caseGrab!=[] and caseGrab!=None:
        if joueur==1:
            posafficher=(((560+(caseGrab[0][0]-1)*80)+20),((940-(caseGrab[0][1])*80)+20))
            fenetrejeu.blit(images_jeu[12],posafficher)
            
        if joueur==2:
                posafficher=(((1360-caseGrab[0][0]*80)+20),((140+(caseGrab[0][1]-1)*80)+20))
                fenetrejeu.blit(images_jeu[12],posafficher)
    GREEN = (0, 255, 0)
    if Grab[0][0]==True:
        for gardien in pieces[0][0]:
            if gardien.couleur == 1:
                gardiengrab=gardien
        casegardien=(gardiengrab.position[1],gardiengrab.position[0])
        casepiece=(Grab[0][1].position[1],Grab[0][1].position[0])
        if joueur==1:
            posgardienafficher=(((565+(casegardien[0]-1)*80)+35),((935-(casegardien[1])*80)+45))
            pospieceafficher=(((565+(casepiece[0]-1)*80)+35),((935-(casepiece[1])*80)+45))
        if joueur==2:
            posgardienafficher=(((1365-casegardien[0]*80)+35.5),((135+(casegardien[1]-1)*80)+45))
            pospieceafficher=(((1365-casepiece[0]*80)+35.5),((135+(casepiece[1]-1)*80)+45))
        pygame.draw.line(fenetrejeu, GREEN, (posgardienafficher), (pospieceafficher), 3)
        pygame.draw.circle(fenetrejeu, GREEN, posgardienafficher, 8, 8)
        pygame.draw.circle(fenetrejeu, GREEN, pospieceafficher, 8, 8)
    if Grab[1][0]==True:
        for gardien in pieces[0][0]:
            if gardien.couleur == 2:
                gardiengrab=gardien
        casegardien=(gardiengrab.position[1],gardiengrab.position[0])
        casepiece=(Grab[1][1].position[1],Grab[1][1].position[0])
        if joueur==1:
            posgardienafficher=(((550+(casegardien[0]-1)*80)+50),((950-(casegardien[1])*80)+30))
            pospieceafficher=(((550+(casepiece[0]-1)*80)+50),((950-(casepiece[1])*80)+30)) 
        if joueur==2:
            posgardienafficher=(((1365-casegardien[0]*80)+35.5),((135+(casegardien[1]-1)*80)+45))
            pospieceafficher=(((1365-casepiece[0]*80)+35.5),((135+(casepiece[1]-1)*80)+45))
        pygame.draw.line(fenetrejeu, GREEN, (posgardienafficher), (pospieceafficher), 3)
        pygame.draw.circle(fenetrejeu, GREEN, posgardienafficher, 8, 8)
        pygame.draw.circle(fenetrejeu, GREEN, pospieceafficher, 8, 8)
        
def afficherchoixremettre(fenetrejeu,images_jeu,choixremettre):
    """
    affiche le choix du refresh
    """
    n=choixremettre[1][1]
    x=320
    y=320
    for i in range (5):
        k=i+1
        if k==n:
            fenetrejeu.blit(images_jeu[15],(x,y))
            fenetrejeu.blit(images_jeu[16],(x+10,y+10))
            y += 80 
            fenetrejeu.blit(images_jeu[14],(x+10,y+10))
            y += 30
        else:
            y += 120 

def afficherall(afficherplateau,afficherpieces,afficherpts,fenetrejeu,images_jeu,pieces,joueur,caseCanMove,
                clic,piecebase,Echec,caseGrab,Grab,banc,choixremettre,casesRefresh):
    """
    affiche tout
    """
    afficherplateau(fenetrejeu,images_jeu,joueur,banc)
    afficherpieces(fenetrejeu,images_jeu,pieces,joueur)
    afficherpts(fenetrejeu,images_jeu,joueur,caseCanMove,clic,piecebase,casesRefresh)
    afficherdanger(fenetrejeu,images_jeu,pieces,joueur,Echec)
    affichergrab(fenetrejeu,images_jeu,joueur,caseGrab,Grab,pieces)
    if choixremettre[0]==True:
        afficherchoixremettre(fenetrejeu,images_jeu,choixremettre)
    pygame.display.flip()