from Fonctions.Deplacer.caseCanMove import *

def echecOuPas(pieces,joueur):
    """
    Fonction qui vérifie si il y a echec en vérifiant toutes les pièces

    Args:
        pieces (list): liste des pieces
        joueur (int): numero du joueur

    Returns:
        list: liste pour l'echec du joueur 1 et du joueur 2
    """
    Echec=[[False,0],[False,0]]
    #1
    Echec1=(False,0)
    echectest1=False
    caseCanMove=[]
    for a in range (10):
        if echectest1==False:
            for b in range (10):
                if echectest1==False:
                    caseCanMove=[]
                    caseselect=(b+1,a+1)
                    soldats=[]
                    bastions=[]
                    donjons=[]
                    empereurs=[]
                    jumpers=[]
                    chapelains=[]
                    piecebaseok=False
                    br=False
                    for niveau in pieces:
                        if br==True:
                            break
                        for sousniveau in niveau:
                            if br==True:
                                break
                            for piece in sousniveau:
                                if br==True:
                                    break
                                if piece.position[0]==caseselect[1] and piece.position[1]==caseselect[0]:
                                    br=True
                                    if piece.couleur==1:
                                        piecebase=piece
                                        piecebaseok=True
                                        soldats=[]
                                        bastions=[]
                                        donjons=[]
                                        empereurs=[]
                                        jumpers=[]
                                        chapelains=[]
                                        if piece.niveau==1:
                                            soldats=pieces[1][0]
                                        elif piece.niveau==2:
                                            if piece.sousniveau==0:
                                                bastions=pieces[2][0]
                                            elif piece.sousniveau==1:
                                                jumpers=pieces[2][1]
                                        elif piece.niveau==3:
                                            if piece.sousniveau==0:
                                                donjons=pieces[3][0]
                                            elif piece.sousniveau==1:
                                                chapelains=pieces[3][1]
                                        elif piece.niveau==4:
                                            empereurs=pieces[4][0]
            
                    for soldat in soldats:
                        if echectest1==False:
                            piecebase=soldat
                            if soldat.position[0] == caseselect[1] and soldat.position[1] == caseselect[0]:
                                caseCanMove = [(caseselect[0]+1,caseselect[1]+1),
                                    (caseselect[0]+1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1),
                                    (caseselect[0]-1,caseselect[1]+1)]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                

                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[c-i]
                                    i += 1
                            
                                for case in caseCanMove:
                                    for gardien in pieces[0][0]:
                                        if gardien.couleur!=piecebase.couleur:
                                            if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                echectest1=True

                    for bastion in bastions:
                        if echectest1==False:
                            piecebase=bastion
                            if bastion.position[0] == caseselect[1] and bastion.position[1] == caseselect[0]:
                                caseCanMove = [[(caseselect[0],caseselect[1]-1),(caseselect[0]-1,caseselect[1]),
                                    (caseselect[0],caseselect[1]+1),(caseselect[0]+1,caseselect[1])],
                                    [(caseselect[0],caseselect[1]-2),(caseselect[0]-2,caseselect[1]),
                                    (caseselect[0],caseselect[1]+2),(caseselect[0]+2,caseselect[1])]]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1                      
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau==piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    i += 1   

                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case0=caseselect[0]
                                                        elif case[0]==caseselect[0]+1:
                                                            case0=caseselect[0]+2
                                                        elif case[0]==caseselect[0]-1:
                                                            case0=caseselect[0]-2
                                                        if case[1]==caseselect[1]:
                                                            case1=caseselect[1]
                                                        elif case[1]==caseselect[1]+1:
                                                            case1=caseselect[1]+2
                                                        elif case[1]==caseselect[1]-1:
                                                            case1=caseselect[1]-2
                                                        for casesupp in caseCanMove[1]:
                                                            if casesupp==(case0,case1):
                                                                caseCanMove[1].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case0=caseselect[0]
                                                            elif case[0]==caseselect[0]+1:
                                                                case0=caseselect[0]+2
                                                            elif case[0]==caseselect[0]-1:
                                                                case0=caseselect[0]-2
                                                            if case[1]==caseselect[1]:
                                                                case1=caseselect[1]
                                                            elif case[1]==caseselect[1]+1:
                                                                case1=caseselect[1]+2
                                                            elif case[1]==caseselect[1]-1:
                                                                case1=caseselect[1]-2
                                                            for casesupp in caseCanMove[1]:
                                                                if casesupp==(case0,case1):
                                                                    caseCanMove[1].remove(casesupp)
                                for surcase in caseCanMove:
                                    for case in surcase:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest1=True

                    for donjon in donjons:
                        if echectest1==False:
                            piecebase=donjon
                            if donjon.position[0] == caseselect[1] and donjon.position[1] == caseselect[0]:
                                caseCanMove = [[(caseselect[0],caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]),
                                    (caseselect[0]-1,caseselect[1]+1),(caseselect[0],caseselect[1]+1),(caseselect[0]+1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]),
                                    (caseselect[0]+1,caseselect[1]-1)],[(caseselect[0],caseselect[1]-2),(caseselect[0]-2,caseselect[1]-2),(caseselect[0]-2,caseselect[1]),
                                    (caseselect[0]-2,caseselect[1]+2),(caseselect[0],caseselect[1]+2),(caseselect[0]+2,caseselect[1]+2),(caseselect[0]+2,caseselect[1]),
                                    (caseselect[0]+2,caseselect[1]-2)],[(caseselect[0],caseselect[1]-3),(caseselect[0]-3,caseselect[1]-3),(caseselect[0]-3,caseselect[1]),
                                    (caseselect[0]-3,caseselect[1]+3),(caseselect[0],caseselect[1]+3),(caseselect[0]+3,caseselect[1]+3),(caseselect[0]+3,caseselect[1]),
                                    (caseselect[0]+3,caseselect[1]-3)]]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i+=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    i += 1   

                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case01=caseselect[0]
                                                            case02=caseselect[0]
                                                        elif case[0]==caseselect[0]+1:
                                                            case01=caseselect[0]+2
                                                            case02=caseselect[0]+3
                                                        elif case[0]==caseselect[0]-1:
                                                            case01=caseselect[0]-2
                                                            case02=caseselect[0]-3
                                                        if case[1]==caseselect[1]:
                                                            case11=caseselect[1]
                                                            case12=caseselect[1]
                                                        elif case[1]==caseselect[1]+1:
                                                            case11=caseselect[1]+2
                                                            case12=caseselect[1]+3
                                                        elif case[1]==caseselect[1]-1:
                                                            case11=caseselect[1]-2
                                                            case12=caseselect[1]-3
                                                        for casesupp in caseCanMove[1]:
                                                            if casesupp==(case01,case11):
                                                                caseCanMove[1].remove(casesupp)
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case01=caseselect[0]
                                                                case02=caseselect[0]
                                                            elif case[0]==caseselect[0]+1:
                                                                case01=caseselect[0]+2
                                                                case02=caseselect[0]+3
                                                            elif case[0]==caseselect[0]-1:
                                                                case01=caseselect[0]-2
                                                                case02=caseselect[0]-3
                                                            if case[1]==caseselect[1]:
                                                                case11=caseselect[1]
                                                                case12=caseselect[1]
                                                            elif case[1]==caseselect[1]+1:
                                                                case11=caseselect[1]+2
                                                                case12=caseselect[1]+3
                                                            elif case[1]==caseselect[1]-1:
                                                                case11=caseselect[1]-2
                                                                case12=caseselect[1]-3
                                                            for casesupp in caseCanMove[1]:
                                                                if casesupp==(case01,case11):
                                                                    caseCanMove[1].remove(casesupp)
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)

                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case02=caseselect[0]
                                                        elif case[0]==caseselect[0]+2:
                                                            case02=caseselect[0]+3
                                                        elif case[0]==caseselect[0]-2:
                                                            case02=caseselect[0]-3
                                                        if case[1]==caseselect[1]:
                                                            case12=caseselect[1]
                                                        elif case[1]==caseselect[1]+2:
                                                            case12=caseselect[1]+3
                                                        elif case[1]==caseselect[1]-2:
                                                            case12=caseselect[1]-3
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case02=caseselect[0]
                                                            elif case[0]==caseselect[0]+2:
                                                                case02=caseselect[0]+3
                                                            elif case[0]==caseselect[0]-2:
                                                                case02=caseselect[0]-3
                                                            if case[1]==caseselect[1]:
                                                                case12=caseselect[1]
                                                            elif case[1]==caseselect[1]+2:
                                                                case12=caseselect[1]+3
                                                            elif case[1]==caseselect[1]-2:
                                                                case12=caseselect[1]-3
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)
                                caseCanMove[0]=[]

                                for surcase in caseCanMove:
                                    for case in surcase:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest1=True

                    for empereur in empereurs:
                        if echectest1==False:
                            piecebase=empereur
                            if empereur.position[0] == caseselect[1] and empereur.position[1] == caseselect[0]:
                                caseCanMove = [[(caseselect[0],caseselect[1]+1),(caseselect[0]+1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]),
                (caseselect[0]+1,caseselect[1]-1),(caseselect[0],caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]),
                (caseselect[0]-1,caseselect[1]+1)],[(caseselect[0],caseselect[1]+2),(caseselect[0]+2,caseselect[1]+2),(caseselect[0]+2,caseselect[1]),
                (caseselect[0]+2,caseselect[1]-2),(caseselect[0],caseselect[1]-2),(caseselect[0]-2,caseselect[1]-2),(caseselect[0]-2,caseselect[1]),
                (caseselect[0]-2,caseselect[1]+2)],[(caseselect[0],caseselect[1]+3),(caseselect[0]+3,caseselect[1]+3),(caseselect[0]+3,caseselect[1]),
                (caseselect[0]+3,caseselect[1]-3),(caseselect[0],caseselect[1]-3),(caseselect[0]-3,caseselect[1]-3),(caseselect[0]-3,caseselect[1]),
                (caseselect[0]-3,caseselect[1]+3)],[(caseselect[0],caseselect[1]+4),(caseselect[0]+4,caseselect[1]+4),(caseselect[0]+4,caseselect[1]),
                (caseselect[0]+4,caseselect[1]-4),(caseselect[0],caseselect[1]-4),(caseselect[0]-4,caseselect[1]-4),(caseselect[0]-4,caseselect[1]),
                (caseselect[0]-4,caseselect[1]+4)]]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i +=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i +=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i +=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[3]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[3]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case01=caseselect[0]
                                                            case02=caseselect[0]
                                                            case03=caseselect[0]
                                                        elif case[0]==caseselect[0]+1:
                                                            case01=caseselect[0]+2
                                                            case02=caseselect[0]+3
                                                            case03=caseselect[0]+4
                                                        elif case[0]==caseselect[0]-1:
                                                            case01=caseselect[0]-2
                                                            case02=caseselect[0]-3
                                                            case03=caseselect[0]-4
                                                        if case[1]==caseselect[1]:
                                                            case11=caseselect[1]
                                                            case12=caseselect[1]
                                                            case13=caseselect[1]
                                                        elif case[1]==caseselect[1]+1:
                                                            case11=caseselect[1]+2
                                                            case12=caseselect[1]+3
                                                            case13=caseselect[1]+4
                                                        elif case[1]==caseselect[1]-1:
                                                            case11=caseselect[1]-2
                                                            case12=caseselect[1]-3
                                                            case13=caseselect[1]-4
                                                        for casesupp in caseCanMove[1]:
                                                            if casesupp==(case01,case11):
                                                                caseCanMove[1].remove(casesupp)
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                        for casesupp in caseCanMove[3]:
                                                            if casesupp==(case03,case13):
                                                                caseCanMove[3].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case01=caseselect[0]
                                                                case02=caseselect[0]
                                                                case03=caseselect[0]
                                                            elif case[0]==caseselect[0]+1:
                                                                case01=caseselect[0]+2
                                                                case02=caseselect[0]+3
                                                                case03=caseselect[0]+4
                                                            elif case[0]==caseselect[0]-1:
                                                                case01=caseselect[0]-2
                                                                case02=caseselect[0]-3
                                                                case03=caseselect[0]-4
                                                            if case[1]==caseselect[1]:
                                                                case11=caseselect[1]
                                                                case12=caseselect[1]
                                                                case13=caseselect[1]
                                                            elif case[1]==caseselect[1]+1:
                                                                case11=caseselect[1]+2
                                                                case12=caseselect[1]+3
                                                                case13=caseselect[1]+4
                                                            elif case[1]==caseselect[1]-1:
                                                                case11=caseselect[1]-2
                                                                case12=caseselect[1]-3
                                                                case13=caseselect[1]-4
                                                            for casesupp in caseCanMove[1]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[1].remove(casesupp)
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)
                                                            for casesupp in caseCanMove[3]:
                                                                if casesupp==(case03,case13):
                                                                    caseCanMove[3].remove(casesupp)

                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case02=caseselect[0]
                                                            case03=caseselect[0]
                                                        elif case[0]==caseselect[0]+2:
                                                            case02=caseselect[0]+3
                                                            case03=caseselect[0]+4
                                                        elif case[0]==caseselect[0]-2:
                                                            case02=caseselect[0]-3
                                                            case03=caseselect[0]-4
                                                        if case[1]==caseselect[1]:
                                                            case12=caseselect[1]
                                                            case13=caseselect[1]
                                                        elif case[1]==caseselect[1]+2:
                                                            case12=caseselect[1]+3
                                                            case13=caseselect[1]+4
                                                        elif case[1]==caseselect[1]-2:
                                                            case12=caseselect[1]-3
                                                            case13=caseselect[1]-4
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                        for casesupp in caseCanMove[3]:
                                                            if casesupp==(case03,case13):
                                                                caseCanMove[3].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case02=caseselect[0]
                                                                case03=caseselect[0]
                                                            elif case[0]==caseselect[0]+2:
                                                                case02=caseselect[0]+3
                                                                case03=caseselect[0]+4
                                                            elif case[0]==caseselect[0]-2:
                                                                case02=caseselect[0]-3
                                                                case03=caseselect[0]-4
                                                            if case[1]==caseselect[1]:
                                                                case12=caseselect[1]
                                                                case13=caseselect[1]
                                                            elif case[1]==caseselect[1]+2:
                                                                case12=caseselect[1]+3
                                                                case13=caseselect[1]+4
                                                            elif case[1]==caseselect[1]-2:
                                                                case12=caseselect[1]-3
                                                                case13=caseselect[1]-4
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case03,case13):
                                                                    caseCanMove[3].remove(casesupp)

                                for case in caseCanMove[2]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case03=caseselect[0]
                                                        elif case[0]==caseselect[0]+3:
                                                            case03=caseselect[0]+4
                                                        elif case[0]==caseselect[0]-3:
                                                            case03=caseselect[0]-4
                                                        if case[1]==caseselect[1]:
                                                            case13=caseselect[1]
                                                        elif case[1]==caseselect[1]+3:
                                                            case13=caseselect[1]+4
                                                        elif case[1]==caseselect[1]-3:
                                                            case13=caseselect[1]-4
                                                        for casesupp in caseCanMove[3]:
                                                            if casesupp==(case03,case13):
                                                                caseCanMove[3].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case03=caseselect[0]
                                                            elif case[0]==caseselect[0]+3:
                                                                case03=caseselect[0]+4
                                                            elif case[0]==caseselect[0]-3:
                                                                case03=caseselect[0]-4
                                                            if case[1]==caseselect[1]:
                                                                case13=caseselect[1]
                                                            elif case[1]==caseselect[1]+3:
                                                                case13=caseselect[1]+4
                                                            elif case[1]==caseselect[1]-3:
                                                                case13=caseselect[1]-4
                                                            for casesupp in caseCanMove[3]:
                                                                if casesupp==(case03,case13):
                                                                    caseCanMove[3].remove(casesupp)
                                
                                for surcase in caseCanMove:
                                    for case in surcase:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest1=True
                                                    
                    for jumper in jumpers:
                        if echectest1==False:
                            piecebase=jumper
                            if jumper.position[0] == caseselect[1] and jumper.position[1] == caseselect[0]:
                                caseCanMove = [[[(caseselect[0],caseselect[1]+1)],[(caseselect[0],caseselect[1]+2)],
                                                [(caseselect[0]-1,caseselect[1]+3),(caseselect[0]+1,caseselect[1]+3),
                                                    (caseselect[0]-1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]+1)]],
                                                [[(caseselect[0]+1,caseselect[1])],[(caseselect[0]+2,caseselect[1])],
                                                [(caseselect[0]+3,caseselect[1]+1),(caseselect[0]+3,caseselect[1]-1),
                                                    (caseselect[0]+1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]-1)]],
                                                [[(caseselect[0],caseselect[1]-1)],[(caseselect[0],caseselect[1]-2)],
                                                [(caseselect[0]+1,caseselect[1]-3),(caseselect[0]-1,caseselect[1]-3),
                                                    (caseselect[0]+1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1)]],
                                                [[(caseselect[0]-1,caseselect[1])],[(caseselect[0]-2,caseselect[1])],
                                                [(caseselect[0]-3,caseselect[1]+1),(caseselect[0]-3,caseselect[1]-1),
                                                    (caseselect[0]-1,caseselect[1]+1),(caseselect[0]-1,caseselect[1]-1)]]]
                        
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    tous=typecase[2]
                                    c=0
                                    for case in tous:
                                        if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                            caseAsupprimer.append(c)
                                        c += 1
                                    i=0
                                    for c in caseAsupprimer:
                                        del typecase[2][c-i]
                                        i += 1
                                
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    c=0
                                    for case in typecase[2]:
                                        ok=False
                                        for niveaux in pieces:
                                            for sousniveaux in niveaux:
                                                for piece in sousniveaux:
                                                    if ok==False:
                                                        if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                            if piece.couleur==piecebase.couleur:
                                                                if piece.niveau==piecebase.niveau:
                                                                    if piece.sousniveau!=piecebase.sousniveau:
                                                                        caseAsupprimer.append(c)
                                                                        ok=True
                                                                else:
                                                                    caseAsupprimer.append(c)
                                                                    ok=True

                                        c += 1

                                    i=0
                                    for k in caseAsupprimer:
                                        del typecase[2][k-i]
                                        i += 1

                                typesAsupprimer = []
                                i=0
                                for types in caseCanMove:
                                    sup=False
                                    semicase=types[1][0]
                                    sautcase=types[0][0]
                                    if len(types[2])==0:
                                        typesAsupprimer.append(i)
                                        sup=True
                                    if sup==False:
                                        if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                                            typesAsupprimer.append(i)
                                            sup=True
                                        else:
                                            semi=False
                                            for niveau in pieces:
                                                if semi==False:
                                                    for sousniveau in niveau:
                                                        if semi==False:
                                                            for piece in sousniveau:
                                                                if semi==False:
                                                                    if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                                        semi=True
                                            if semi==True:
                                                typesAsupprimer.append(i)
                                                sup=True

                                    if sup==False:
                                        saut=False
                                        for niveau in pieces:
                                            if saut==False:
                                                for sousniveau in niveau:
                                                    if saut==False:
                                                        for piece in sousniveau:
                                                            if saut==False:
                                                                if piece.position[1]==sautcase[0] and piece.position[0]==sautcase[1]:
                                                                    saut=True
                                        if saut==False:
                                            typesAsupprimer.append(i)
                                            sup=True
                                    i += 1
                                i=0
                                for t in typesAsupprimer:
                                    del caseCanMove[t-i]
                                    i += 1

                                for type in caseCanMove:
                                    for case in type[2]:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest1=True

                    for chapelain in chapelains:
                        if echectest1==False:
                            piecebase=chapelain
                            if chapelain.position[0] == caseselect[1] and chapelain.position[1] == caseselect[0]:
                                caseCanMove = [[[(caseselect[0],caseselect[1]+2)],
                                            [(caseselect[0]-1,caseselect[1]+3),(caseselect[0]+1,caseselect[1]+3),(caseselect[0],caseselect[1]+1)]],
                                                [[(caseselect[0]+2,caseselect[1])],
                                                [(caseselect[0]+3,caseselect[1]+1),(caseselect[0]+3,caseselect[1]-1),(caseselect[0]+1,caseselect[1])]],
                                                [[(caseselect[0],caseselect[1]-2)],
                                                [(caseselect[0]+1,caseselect[1]-3),(caseselect[0]-1,caseselect[1]-3),(caseselect[0],caseselect[1]-1)]],
                                                [[(caseselect[0]-2,caseselect[1])],
                                                [(caseselect[0]-3,caseselect[1]+1),(caseselect[0]-3,caseselect[1]-1),(caseselect[0]-1,caseselect[1])]]]
                        
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    tous=typecase[1]
                                    c=0
                                    for case in tous:
                                        if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                            caseAsupprimer.append(c)
                                        c += 1
                                    i=0
                                    for c in caseAsupprimer:
                                        del typecase[1][c-i]
                                        i += 1
                                
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    tous=typecase[1]
                                    c=0
                                    for case in tous:
                                        fbr=False
                                        for niveaux in pieces:
                                            if br==True:
                                                break
                                            for sousniveaux in niveaux:
                                                if br==True:
                                                    break
                                                for piece in sousniveaux:
                                                    if br==True:
                                                        break
                                                    if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                        br=True
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.niveau==piecebase.niveau:
                                                                if piece.sousniveau!=piecebase.sousniveau:
                                                                    caseAsupprimer.append(c)
                                                            else:
                                                                caseAsupprimer.append(c)
                                        c += 1

                                    i=0
                                    for c in caseAsupprimer:
                                        del typecase[1][c-i]
                                        i += 1

                                typesAsupprimer = []
                                i=0
                                for types in caseCanMove:
                                    sup=False
                                    semicase=types[0][0]
                                    if len(types[1])==0:
                                        typesAsupprimer.append(i)
                                        sup=True
                                    if sup==False:
                                        if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                                            typesAsupprimer.append(i)
                                            sup=True
                                        else:
                                            semi=False
                                            for niveau in pieces:
                                                if semi==False:
                                                    for sousniveau in niveau:
                                                        if semi==False:
                                                            for piece in sousniveau:
                                                                if semi==False:
                                                                    if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                                        semi=True
                                            if semi==True:
                                                typesAsupprimer.append(i)
                                                sup=True
                                    i += 1
                                i=0
                                for t in typesAsupprimer:
                                    del caseCanMove[t-i]
                                    i += 1

                                
                                for type in caseCanMove:
                                    for case in type[1]:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest1=True


                    if echectest1==True:
                        Echec1=[True,2]
    
    #2
    Echec2=(False,0)
    echectest2=False
    caseCanMove=[]
    for a in range (10):
        if echectest2==False:
            for b in range (10):
                if echectest2==False:
                    caseCanMove=[]
                    caseselect=(b+1,a+1)
                    soldats=[]
                    bastions=[]
                    donjons=[]
                    empereurs=[]
                    jumpers=[]
                    chapelains=[]
                    piecebaseok=False
                    br=False
                    for niveau in pieces:
                        if br==True:
                            break
                        for sousniveau in niveau:
                            if br==True:
                                break
                            for piece in sousniveau:
                                if br==True:
                                    break
                                if piece.position[0]==caseselect[1] and piece.position[1]==caseselect[0]:
                                    br=True
                                    if piece.couleur==2:
                                        piecebase=piece
                                        piecebaseok=True
                                        soldats=[]
                                        bastions=[]
                                        donjons=[]
                                        empereurs=[]
                                        jumpers=[]
                                        chapelains=[]
                                        if piece.niveau==1:
                                            soldats=pieces[1][0]
                                        elif piece.niveau==2:
                                            if piece.sousniveau==0:
                                                bastions=pieces[2][0]
                                            elif piece.sousniveau==1:
                                                jumpers=pieces[2][1]
                                        elif piece.niveau==3:
                                            if piece.sousniveau==0:
                                                donjons=pieces[3][0]
                                            elif piece.sousniveau==1:
                                                chapelains=pieces[3][1]
                                        elif piece.niveau==4:
                                            empereurs=pieces[4][0]
            
                    for soldat in soldats:
                        if echectest2==False:
                            piecebase=soldat
                            if soldat.position[0] == caseselect[1] and soldat.position[1] == caseselect[0]:
                                caseCanMove = [(caseselect[0]+1,caseselect[1]+1),
                                    (caseselect[0]+1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1),
                                    (caseselect[0]-1,caseselect[1]+1)]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1

                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[c-i]
                                    i += 1
                            
                                for case in caseCanMove:
                                    for gardien in pieces[0][0]:
                                        if gardien.couleur!=piecebase.couleur:
                                            if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                echectest2=True

                    for bastion in bastions:
                        if echectest2==False:
                            piecebase=bastion
                            if bastion.position[0] == caseselect[1] and bastion.position[1] == caseselect[0]:
                                caseCanMove = [[(caseselect[0],caseselect[1]-1),(caseselect[0]-1,caseselect[1]),
                                    (caseselect[0],caseselect[1]+1),(caseselect[0]+1,caseselect[1])],
                                    [(caseselect[0],caseselect[1]-2),(caseselect[0]-2,caseselect[1]),
                                     (caseselect[0],caseselect[1]+2),(caseselect[0]+2,caseselect[1])]]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1                      
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau==piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    i += 1   

                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case0=caseselect[0]
                                                        elif case[0]==caseselect[0]+1:
                                                            case0=caseselect[0]+2
                                                        elif case[0]==caseselect[0]-1:
                                                            case0=caseselect[0]-2
                                                        if case[1]==caseselect[1]:
                                                            case1=caseselect[1]
                                                        elif case[1]==caseselect[1]+1:
                                                            case1=caseselect[1]+2
                                                        elif case[1]==caseselect[1]-1:
                                                            case1=caseselect[1]-2
                                                        for casesupp in caseCanMove[1]:
                                                            if casesupp==(case0,case1):
                                                                caseCanMove[1].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case0=caseselect[0]
                                                            elif case[0]==caseselect[0]+1:
                                                                case0=caseselect[0]+2
                                                            elif case[0]==caseselect[0]-1:
                                                                case0=caseselect[0]-2
                                                            if case[1]==caseselect[1]:
                                                                case1=caseselect[1]
                                                            elif case[1]==caseselect[1]+1:
                                                                case1=caseselect[1]+2
                                                            elif case[1]==caseselect[1]-1:
                                                                case1=caseselect[1]-2
                                                            for casesupp in caseCanMove[1]:
                                                                if casesupp==(case0,case1):
                                                                    caseCanMove[1].remove(casesupp)
                                for surcase in caseCanMove:
                                    for case in surcase:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest2=True

                    for donjon in donjons:
                        if echectest2==False:
                            piecebase=donjon
                            if donjon.position[0] == caseselect[1] and donjon.position[1] == caseselect[0]:
                                caseCanMove = [[(caseselect[0],caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]),
                                    (caseselect[0]-1,caseselect[1]+1),(caseselect[0],caseselect[1]+1),(caseselect[0]+1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]),
                                    (caseselect[0]+1,caseselect[1]-1)],[(caseselect[0],caseselect[1]-2),(caseselect[0]-2,caseselect[1]-2),(caseselect[0]-2,caseselect[1]),
                                    (caseselect[0]-2,caseselect[1]+2),(caseselect[0],caseselect[1]+2),(caseselect[0]+2,caseselect[1]+2),(caseselect[0]+2,caseselect[1]),
                                    (caseselect[0]+2,caseselect[1]-2)],[(caseselect[0],caseselect[1]-3),(caseselect[0]-3,caseselect[1]-3),(caseselect[0]-3,caseselect[1]),
                                    (caseselect[0]-3,caseselect[1]+3),(caseselect[0],caseselect[1]+3),(caseselect[0]+3,caseselect[1]+3),(caseselect[0]+3,caseselect[1]),
                                    (caseselect[0]+3,caseselect[1]-3)]]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.sousniveau!=piecebase.sousniveau:
                                                                caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i+=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    i += 1   

                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case01=caseselect[0]
                                                            case02=caseselect[0]
                                                        elif case[0]==caseselect[0]+1:
                                                            case01=caseselect[0]+2
                                                            case02=caseselect[0]+3
                                                        elif case[0]==caseselect[0]-1:
                                                            case01=caseselect[0]-2
                                                            case02=caseselect[0]-3
                                                        if case[1]==caseselect[1]:
                                                            case11=caseselect[1]
                                                            case12=caseselect[1]
                                                        elif case[1]==caseselect[1]+1:
                                                            case11=caseselect[1]+2
                                                            case12=caseselect[1]+3
                                                        elif case[1]==caseselect[1]-1:
                                                            case11=caseselect[1]-2
                                                            case12=caseselect[1]-3
                                                        for casesupp in caseCanMove[1]:
                                                            if casesupp==(case01,case11):
                                                                caseCanMove[1].remove(casesupp)
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case01=caseselect[0]
                                                                case02=caseselect[0]
                                                            elif case[0]==caseselect[0]+1:
                                                                case01=caseselect[0]+2
                                                                case02=caseselect[0]+3
                                                            elif case[0]==caseselect[0]-1:
                                                                case01=caseselect[0]-2
                                                                case02=caseselect[0]-3
                                                            if case[1]==caseselect[1]:
                                                                case11=caseselect[1]
                                                                case12=caseselect[1]
                                                            elif case[1]==caseselect[1]+1:
                                                                case11=caseselect[1]+2
                                                                case12=caseselect[1]+3
                                                            elif case[1]==caseselect[1]-1:
                                                                case11=caseselect[1]-2
                                                                case12=caseselect[1]-3
                                                            for casesupp in caseCanMove[1]:
                                                                if casesupp==(case01,case11):
                                                                    caseCanMove[1].remove(casesupp)
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)

                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case02=caseselect[0]
                                                        elif case[0]==caseselect[0]+2:
                                                            case02=caseselect[0]+3
                                                        elif case[0]==caseselect[0]-2:
                                                            case02=caseselect[0]-3
                                                        if case[1]==caseselect[1]:
                                                            case12=caseselect[1]
                                                        elif case[1]==caseselect[1]+2:
                                                            case12=caseselect[1]+3
                                                        elif case[1]==caseselect[1]-2:
                                                            case12=caseselect[1]-3
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case02=caseselect[0]
                                                            elif case[0]==caseselect[0]+2:
                                                                case02=caseselect[0]+3
                                                            elif case[0]==caseselect[0]-2:
                                                                case02=caseselect[0]-3
                                                            if case[1]==caseselect[1]:
                                                                case12=caseselect[1]
                                                            elif case[1]==caseselect[1]+2:
                                                                case12=caseselect[1]+3
                                                            elif case[1]==caseselect[1]-2:
                                                                case12=caseselect[1]-3
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)
                                
                                caseCanMove[0]=[]

                                for surcase in caseCanMove:
                                    for case in surcase:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest2=True

                    for empereur in empereurs:
                        if echectest2==False:
                            piecebase=empereur
                            if empereur.position[0] == caseselect[1] and empereur.position[1] == caseselect[0]:
                                caseCanMove = [[(caseselect[0],caseselect[1]+1),(caseselect[0]+1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]),
                (caseselect[0]+1,caseselect[1]-1),(caseselect[0],caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]),
                (caseselect[0]-1,caseselect[1]+1)],[(caseselect[0],caseselect[1]+2),(caseselect[0]+2,caseselect[1]+2),(caseselect[0]+2,caseselect[1]),
                (caseselect[0]+2,caseselect[1]-2),(caseselect[0],caseselect[1]-2),(caseselect[0]-2,caseselect[1]-2),(caseselect[0]-2,caseselect[1]),
                (caseselect[0]-2,caseselect[1]+2)],[(caseselect[0],caseselect[1]+3),(caseselect[0]+3,caseselect[1]+3),(caseselect[0]+3,caseselect[1]),
                (caseselect[0]+3,caseselect[1]-3),(caseselect[0],caseselect[1]-3),(caseselect[0]-3,caseselect[1]-3),(caseselect[0]-3,caseselect[1]),
                (caseselect[0]-3,caseselect[1]+3)],[(caseselect[0],caseselect[1]+4),(caseselect[0]+4,caseselect[1]+4),(caseselect[0]+4,caseselect[1]),
                (caseselect[0]+4,caseselect[1]-4),(caseselect[0],caseselect[1]-4),(caseselect[0]-4,caseselect[1]-4),(caseselect[0]-4,caseselect[1]),
                (caseselect[0]-4,caseselect[1]+4)]]
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i +=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[0][c-i]
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i +=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[1][c-i]
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1

                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[2]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i +=1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[2][c-i]
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[3]:
                                    if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                        caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                i=0
                                caseAsupprimer=[]
                                for case in caseCanMove[3]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.niveau == piecebase.niveau:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                                    else:
                                                        if piece.couleur==piecebase.couleur:
                                                            caseAsupprimer.append(i)
                                    i += 1
                                i=0
                                for c in caseAsupprimer:
                                    del caseCanMove[3][c-i]
                                    i += 1
                                
                                for case in caseCanMove[0]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case01=caseselect[0]
                                                            case02=caseselect[0]
                                                            case03=caseselect[0]
                                                        elif case[0]==caseselect[0]+1:
                                                            case01=caseselect[0]+2
                                                            case02=caseselect[0]+3
                                                            case03=caseselect[0]+4
                                                        elif case[0]==caseselect[0]-1:
                                                            case01=caseselect[0]-2
                                                            case02=caseselect[0]-3
                                                            case03=caseselect[0]-4
                                                        if case[1]==caseselect[1]:
                                                            case11=caseselect[1]
                                                            case12=caseselect[1]
                                                            case13=caseselect[1]
                                                        elif case[1]==caseselect[1]+1:
                                                            case11=caseselect[1]+2
                                                            case12=caseselect[1]+3
                                                            case13=caseselect[1]+4
                                                        elif case[1]==caseselect[1]-1:
                                                            case11=caseselect[1]-2
                                                            case12=caseselect[1]-3
                                                            case13=caseselect[1]-4
                                                        for casesupp in caseCanMove[1]:
                                                            if casesupp==(case01,case11):
                                                                caseCanMove[1].remove(casesupp)
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                        for casesupp in caseCanMove[3]:
                                                            if casesupp==(case03,case13):
                                                                caseCanMove[3].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case01=caseselect[0]
                                                                case02=caseselect[0]
                                                                case03=caseselect[0]
                                                            elif case[0]==caseselect[0]+1:
                                                                case01=caseselect[0]+2
                                                                case02=caseselect[0]+3
                                                                case03=caseselect[0]+4
                                                            elif case[0]==caseselect[0]-1:
                                                                case01=caseselect[0]-2
                                                                case02=caseselect[0]-3
                                                                case03=caseselect[0]-4
                                                            if case[1]==caseselect[1]:
                                                                case11=caseselect[1]
                                                                case12=caseselect[1]
                                                                case13=caseselect[1]
                                                            elif case[1]==caseselect[1]+1:
                                                                case11=caseselect[1]+2
                                                                case12=caseselect[1]+3
                                                                case13=caseselect[1]+4
                                                            elif case[1]==caseselect[1]-1:
                                                                case11=caseselect[1]-2
                                                                case12=caseselect[1]-3
                                                                case13=caseselect[1]-4
                                                            for casesupp in caseCanMove[1]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[1].remove(casesupp)
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)
                                                            for casesupp in caseCanMove[3]:
                                                                if casesupp==(case03,case13):
                                                                    caseCanMove[3].remove(casesupp)

                                for case in caseCanMove[1]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case02=caseselect[0]
                                                            case03=caseselect[0]
                                                        elif case[0]==caseselect[0]+2:
                                                            case02=caseselect[0]+3
                                                            case03=caseselect[0]+4
                                                        elif case[0]==caseselect[0]-2:
                                                            case02=caseselect[0]-3
                                                            case03=caseselect[0]-4
                                                        if case[1]==caseselect[1]:
                                                            case12=caseselect[1]
                                                            case13=caseselect[1]
                                                        elif case[1]==caseselect[1]+2:
                                                            case12=caseselect[1]+3
                                                            case13=caseselect[1]+4
                                                        elif case[1]==caseselect[1]-2:
                                                            case12=caseselect[1]-3
                                                            case13=caseselect[1]-4
                                                        for casesupp in caseCanMove[2]:
                                                            if casesupp==(case02,case12):
                                                                caseCanMove[2].remove(casesupp)
                                                        for casesupp in caseCanMove[3]:
                                                            if casesupp==(case03,case13):
                                                                caseCanMove[3].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case02=caseselect[0]
                                                                case03=caseselect[0]
                                                            elif case[0]==caseselect[0]+2:
                                                                case02=caseselect[0]+3
                                                                case03=caseselect[0]+4
                                                            elif case[0]==caseselect[0]-2:
                                                                case02=caseselect[0]-3
                                                                case03=caseselect[0]-4
                                                            if case[1]==caseselect[1]:
                                                                case12=caseselect[1]
                                                                case13=caseselect[1]
                                                            elif case[1]==caseselect[1]+2:
                                                                case12=caseselect[1]+3
                                                                case13=caseselect[1]+4
                                                            elif case[1]==caseselect[1]-2:
                                                                case12=caseselect[1]-3
                                                                case13=caseselect[1]-4
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case02,case12):
                                                                    caseCanMove[2].remove(casesupp)
                                                            for casesupp in caseCanMove[2]:
                                                                if casesupp==(case03,case13):
                                                                    caseCanMove[3].remove(casesupp)

                                for case in caseCanMove[2]:
                                    br=False
                                    for niveau in pieces:
                                        if br==True:
                                            break
                                        for sousniveau in niveau:
                                            if br==True:
                                                break
                                            for piece in sousniveau:
                                                if br==True:
                                                    break
                                                if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                    br=True
                                                    if piece.couleur!=piecebase.couleur:
                                                        if case[0]==caseselect[0]:
                                                            case03=caseselect[0]
                                                        elif case[0]==caseselect[0]+3:
                                                            case03=caseselect[0]+4
                                                        elif case[0]==caseselect[0]-3:
                                                            case03=caseselect[0]-4
                                                        if case[1]==caseselect[1]:
                                                            case13=caseselect[1]
                                                        elif case[1]==caseselect[1]+3:
                                                            case13=caseselect[1]+4
                                                        elif case[1]==caseselect[1]-3:
                                                            case13=caseselect[1]-4
                                                        for casesupp in caseCanMove[3]:
                                                            if casesupp==(case03,case13):
                                                                caseCanMove[3].remove(casesupp)
                                                    if piece.couleur==piecebase.couleur:
                                                        if piece.niveau==piecebase.niveau and piece.sousniveau==piecebase.sousniveau:
                                                            if case[0]==caseselect[0]:
                                                                case03=caseselect[0]
                                                            elif case[0]==caseselect[0]+3:
                                                                case03=caseselect[0]+4
                                                            elif case[0]==caseselect[0]-3:
                                                                case03=caseselect[0]-4
                                                            if case[1]==caseselect[1]:
                                                                case13=caseselect[1]
                                                            elif case[1]==caseselect[1]+3:
                                                                case13=caseselect[1]+4
                                                            elif case[1]==caseselect[1]-3:
                                                                case13=caseselect[1]-4
                                                            for casesupp in caseCanMove[3]:
                                                                if casesupp==(case03,case13):
                                                                    caseCanMove[3].remove(casesupp)
                                
                                for surcase in caseCanMove:
                                    for case in surcase:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest2=True
                                                    
                    for jumper in jumpers:
                        if echectest2==False:
                            piecebase=jumper
                            if jumper.position[0] == caseselect[1] and jumper.position[1] == caseselect[0]:
                                caseCanMove = [[[(caseselect[0],caseselect[1]+1)],[(caseselect[0],caseselect[1]+2)],
                                                [(caseselect[0]-1,caseselect[1]+3),(caseselect[0]+1,caseselect[1]+3),
                                                    (caseselect[0]-1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]+1)]],
                                                [[(caseselect[0]+1,caseselect[1])],[(caseselect[0]+2,caseselect[1])],
                                                [(caseselect[0]+3,caseselect[1]+1),(caseselect[0]+3,caseselect[1]-1),
                                                    (caseselect[0]+1,caseselect[1]+1),(caseselect[0]+1,caseselect[1]-1)]],
                                                [[(caseselect[0],caseselect[1]-1)],[(caseselect[0],caseselect[1]-2)],
                                                [(caseselect[0]+1,caseselect[1]-3),(caseselect[0]-1,caseselect[1]-3),
                                                    (caseselect[0]+1,caseselect[1]-1),(caseselect[0]-1,caseselect[1]-1)]],
                                                [[(caseselect[0]-1,caseselect[1])],[(caseselect[0]-2,caseselect[1])],
                                                [(caseselect[0]-3,caseselect[1]+1),(caseselect[0]-3,caseselect[1]-1),
                                                    (caseselect[0]-1,caseselect[1]+1),(caseselect[0]-1,caseselect[1]-1)]]]
                        
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    tous=typecase[2]
                                    c=0
                                    for case in tous:
                                        if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                            caseAsupprimer.append(c)
                                        c += 1
                                    i=0
                                    for c in caseAsupprimer:
                                        del typecase[2][c-i]
                                        i += 1
                                
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    c=0
                                    for case in typecase[2]:
                                        ok=False
                                        for niveaux in pieces:
                                            for sousniveaux in niveaux:
                                                for piece in sousniveaux:
                                                    if ok==False:
                                                        if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                            if piece.couleur==piecebase.couleur:
                                                                if piece.niveau==piecebase.niveau:
                                                                    if piece.sousniveau!=piecebase.sousniveau:
                                                                        caseAsupprimer.append(c)
                                                                        ok=True
                                                                else:
                                                                    caseAsupprimer.append(c)
                                                                    ok=True

                                        c += 1

                                    i=0
                                    for k in caseAsupprimer:
                                        del typecase[2][k-i]
                                        i += 1

                                typesAsupprimer = []
                                i=0
                                for types in caseCanMove:
                                    sup=False
                                    semicase=types[1][0]
                                    sautcase=types[0][0]
                                    if len(types[2])==0:
                                        typesAsupprimer.append(i)
                                        sup=True
                                    if sup==False:
                                        if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                                            typesAsupprimer.append(i)
                                            sup=True
                                        else:
                                            semi=False
                                            for niveau in pieces:
                                                if semi==False:
                                                    for sousniveau in niveau:
                                                        if semi==False:
                                                            for piece in sousniveau:
                                                                if semi==False:
                                                                    if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                                        semi=True
                                            if semi==True:
                                                typesAsupprimer.append(i)
                                                sup=True

                                    if sup==False:
                                        saut=False
                                        for niveau in pieces:
                                            if saut==False:
                                                for sousniveau in niveau:
                                                    if saut==False:
                                                        for piece in sousniveau:
                                                            if saut==False:
                                                                if piece.position[1]==sautcase[0] and piece.position[0]==sautcase[1]:
                                                                    saut=True
                                        if saut==False:
                                            typesAsupprimer.append(i)
                                            sup=True
                                    i += 1
                                i=0
                                for t in typesAsupprimer:
                                    del caseCanMove[t-i]
                                    i += 1

                                for type in caseCanMove:
                                    for case in type[2]:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest2=True

                    for chapelain in chapelains:
                        if echectest2==False:
                            piecebase=chapelain
                            if chapelain.position[0] == caseselect[1] and chapelain.position[1] == caseselect[0]:
                                caseCanMove = [[[(caseselect[0],caseselect[1]+2)],
                                            [(caseselect[0]-1,caseselect[1]+3),(caseselect[0]+1,caseselect[1]+3),(caseselect[0],caseselect[1]+1)]],
                                                [[(caseselect[0]+2,caseselect[1])],
                                                [(caseselect[0]+3,caseselect[1]+1),(caseselect[0]+3,caseselect[1]-1),(caseselect[0]+1,caseselect[1])]],
                                                [[(caseselect[0],caseselect[1]-2)],
                                                [(caseselect[0]+1,caseselect[1]-3),(caseselect[0]-1,caseselect[1]-3),(caseselect[0],caseselect[1]-1)]],
                                                [[(caseselect[0]-2,caseselect[1])],
                                                [(caseselect[0]-3,caseselect[1]+1),(caseselect[0]-3,caseselect[1]-1),(caseselect[0]-1,caseselect[1])]]]
                        
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    tous=typecase[1]
                                    c=0
                                    for case in tous:
                                        if case[0]<1 or case[0]>10 or case[1]<1 or case[1]>10:
                                            caseAsupprimer.append(c)
                                        c += 1
                                    i=0
                                    for c in caseAsupprimer:
                                        del typecase[1][c-i]
                                        i += 1
                                
                                for typecase in caseCanMove:
                                    caseAsupprimer=[]
                                    tous=typecase[1]
                                    c=0
                                    for case in tous:
                                        br=False
                                        for niveaux in pieces:
                                            if br==True:
                                                break
                                            for sousniveaux in niveaux:
                                                if br==True:
                                                    break
                                                for piece in sousniveaux:
                                                    if br==True:
                                                        break
                                                    if piece.position[1]==case[0] and piece.position[0]==case[1]:
                                                        br=True
                                                        if piece.couleur==piecebase.couleur:
                                                            if piece.niveau==piecebase.niveau:
                                                                if piece.sousniveau!=piecebase.sousniveau:
                                                                    caseAsupprimer.append(c)
                                                            else:
                                                                caseAsupprimer.append(c)
                                        c += 1

                                    i=0
                                    for c in caseAsupprimer:
                                        del typecase[1][c-i]
                                        i += 1

                                typesAsupprimer = []
                                i=0
                                for types in caseCanMove:
                                    sup=False
                                    semicase=types[0][0]
                                    if len(types[1])==0:
                                        typesAsupprimer.append(i)
                                        sup=True
                                    if sup==False:
                                        if semicase[0]<1 or semicase[0]>10 or semicase[1]<1 or semicase[1]>10:
                                            typesAsupprimer.append(i)
                                            sup=True
                                        else:
                                            semi=False
                                            for niveau in pieces:
                                                if semi==False:
                                                    for sousniveau in niveau:
                                                        if semi==False:
                                                            for piece in sousniveau:
                                                                if semi==False:
                                                                    if piece.position[1]==semicase[0] and piece.position[0]==semicase[1]:
                                                                        semi=True
                                            if semi==True:
                                                typesAsupprimer.append(i)
                                                sup=True
                                    i += 1
                                i=0
                                for t in typesAsupprimer:
                                    del caseCanMove[t-i]
                                    i += 1

                                
                                for type in caseCanMove:
                                    for case in type[1]:
                                        for gardien in pieces[0][0]:
                                            if gardien.couleur!=piecebase.couleur:
                                                if gardien.position[0]==case[1] and gardien.position[1]==case[0]:
                                                    echectest2=True


                    if echectest2==True:
                        Echec2=[True,1]

    return [Echec2,Echec1]

def changercasessiGrab(ok,piecebase,joueur,boitePieces,caseCanMove,cases,pieces,Grab):
    """
    Modifie les cases can move si il ya le grab, c'est a dire interdire les fusions ou les potentitelles cases de mise en echec tout seul

    """
    if ok==True :
        caseAsupprimer=[]
        for soldat in pieces[1][0]:
            if soldat==piecebase:
                ancpossoldat=soldat.position
        for gardien in pieces[0][0]:
            if gardien.couleur==joueur:
                ancposgardien=gardien.position
                gardien.position=ancpossoldat
        i=0
        boitePieces.pieces=pieces
        for case in caseCanMove:
            boitePieces.pieces=pieces
            if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                for niveau in pieces:
                    for sousniveau in niveau:
                        for pieceok in sousniveau:
                            if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                pieceok2=pieceok
                                pieces=boitePieces.remove_piece(pieceok2,pieces)
            for soldat in pieces[1][0]:
                if soldat==piecebase:
                    soldat.position=(case[1],case[0])
            Echectest=echecOuPas(pieces,joueur)
            if Echectest[joueur-1][0]==True:
                caseAsupprimer.append(i)
            for soldat in pieces[1][0]:
                if soldat==piecebase:
                    soldat.position=ancpossoldat
            if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                pieces=boitePieces.add_piece(pieceok2,pieces)
            i += 1
        i=0
        for c in caseAsupprimer:
            del caseCanMove[c-i]
            i += 1
        for gardien in pieces[0][0]:
            if gardien.couleur==joueur:
                gardien.position=ancposgardien
    
    else:
        for niveau in pieces:
            for sousniveau in niveau:
                for piece in sousniveau:
                    if piece==piecebase:
                        ancpospiece=piece.position
        i=0
        boitePieces.pieces=pieces
        if piecebase.niveau==1:
            caseAsupprimer=[]     
            for case in caseCanMove:
                boitePieces.pieces=pieces
                if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                    for niveau in pieces:
                        for sousniveau in niveau:
                            for pieceok in sousniveau:
                                if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                    pieceok2=pieceok
                                    pieces=boitePieces.remove_piece(pieceok2,pieces)
                for niveau in pieces:
                    for sousniveau in niveau:
                        for piece in sousniveau:
                            if piece==piecebase:
                                piece.position=(case[1],case[0])
                Echectest=echecOuPas(pieces,joueur)
                if Echectest[joueur-1][0]==True:
                    caseAsupprimer.append(i)
                for niveau in pieces:
                    for sousniveau in niveau:
                        for piece in sousniveau:
                            if piece==piecebase:
                                piece.position=ancpospiece
                if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                    pieces=boitePieces.add_piece(pieceok2,pieces)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
        else:
            if piecebase.sousniveau==0:
                for surcase in caseCanMove:
                    k=0
                    caseAsupprimer=[]
                    for case in surcase:
                        boitePieces.pieces=pieces
                        if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                            for niveau in pieces:
                                for sousniveau in niveau:
                                    for pieceok in sousniveau:
                                        if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                            pieceok2=pieceok
                                            pieces=boitePieces.remove_piece(pieceok2,pieces)
                        for niveau in pieces:
                            for sousniveau in niveau:
                                for piece in sousniveau:
                                    if piece==piecebase:
                                        piece.position=(case[1],case[0])
                        Echectest=echecOuPas(pieces,joueur)
                        if Echectest[joueur-1][0]==True:
                            caseAsupprimer.append(k)
                        for niveau in pieces:
                            for sousniveau in niveau:
                                for piece in sousniveau:
                                    if piece==piecebase:
                                        piece.position=ancpospiece
                        if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                            pieces=boitePieces.add_piece(pieceok2,pieces)
                        k += 1
                    e=0
                    for c in caseAsupprimer:
                        del surcase[c-e]
                        e += 1
            elif piecebase.sousniveau==1:
                for surcase in caseCanMove[1]:
                    k=0
                    caseAsupprimer=[]
                    for case in surcase:
                        boitePieces.pieces=pieces
                        if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                            for niveau in pieces:
                                for sousniveau in niveau:
                                    for pieceok in sousniveau:
                                        if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                            pieceok2=pieceok
                                            pieces=boitePieces.remove_piece(pieceok2,pieces)
                        for niveau in pieces:
                            for sousniveau in niveau:
                                for piece in sousniveau:
                                    if piece==piecebase:
                                        piece.position=(case[1],case[0])
                        Echectest=echecOuPas(pieces,joueur)
                        if Echectest[joueur-1][0]==True:
                            caseAsupprimer.append(k)
                            ok += 1
                        for niveau in pieces:
                            for sousniveau in niveau:
                                for piece in sousniveau:
                                    if piece==piecebase:
                                        piece.position=ancpospiece
                        if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                            pieces=boitePieces.add_piece(pieceok2,pieces)
                        k += 1
                    e=0
                    for c in caseAsupprimer:
                        del surcase[c-e]
                        e += 1

    return caseCanMove

def changercasessiGrab2(ok,piecebase,joueur,boitePieces,caseCanMove,cases,pieces,Grab):
    if ok==True :
        caseAsupprimer=[]
        for soldat in pieces[1][0]:
            if soldat==piecebase:
                ancpossoldat=soldat.position
        for gardien in pieces[0][0]:
            if gardien.couleur==joueur:
                ancposgardien=gardien.position
                gardien.position=ancpossoldat
        i=0
        boitePieces.pieces=pieces
        for case in caseCanMove:
            if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                for niveau in pieces:
                    for sousniveau in niveau:
                        for pieceok in sousniveau:
                            if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                pieceok2=pieceok
                                pieces=boitePieces.remove_piece(pieceok2,pieces)
            for soldat in pieces[1][0]:
                if soldat==piecebase:
                    soldat.position=(case[1],case[0])
            Echectest=echecOuPas(pieces,joueur)
            if Echectest[joueur-1][0]==True:
                caseAsupprimer.append(i)
            for soldat in pieces[1][0]:
                if soldat==piecebase:
                    soldat.position=ancpossoldat
            if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                pieces=boitePieces.add_piece(pieceok2,pieces)
            i += 1
        i=0
        for c in caseAsupprimer:
            del caseCanMove[c-i]
            i += 1
        for gardien in pieces[0][0]:
            if gardien.couleur==joueur:
                gardien.position=ancposgardien
    
    else:
        for niveau in pieces:
            for sousniveau in niveau:
                for piece in sousniveau:
                    if piece==piecebase:
                        ancpospiece=piece.position
        i=0
        boitePieces.pieces=pieces
        if piecebase.niveau==1:
            caseAsupprimer=[]     
            for case in caseCanMove:
                boitePieces.pieces=pieces
                if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                    for niveau in pieces:
                        for sousniveau in niveau:
                            for pieceok in sousniveau:
                                if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                    pieceok2=pieceok
                                    pieces=boitePieces.remove_piece(pieceok2,pieces)
                for niveau in pieces:
                    for sousniveau in niveau:
                        for piece in sousniveau:
                            if piece==piecebase:
                                piece.position=(case[1],case[0])
                Echectest=echecOuPas(pieces,joueur)
                if Echectest[joueur-1][0]==True:
                    caseAsupprimer.append(i)
                for niveau in pieces:
                    for sousniveau in niveau:
                        for piece in sousniveau:
                            if piece==piecebase:
                                piece.position=ancpospiece
                if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                    pieces=boitePieces.add_piece(pieceok2,pieces)
                i += 1
            i=0
            for c in caseAsupprimer:
                del caseCanMove[c-i]
                i += 1
        else:
            if piecebase.sousniveau==0:
                k=0
                caseAsupprimer=[]
                for case in caseCanMove:
                    boitePieces.pieces=pieces
                    if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                        for niveau in pieces:
                            for sousniveau in niveau:
                                for pieceok in sousniveau:
                                    if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                        pieceok2=pieceok
                                        pieces=boitePieces.remove_piece(pieceok2,pieces)
                    for niveau in pieces:
                        for sousniveau in niveau:
                            for piece in sousniveau:
                                if piece==piecebase:
                                    piece.position=(case[1],case[0])
                    Echectest=echecOuPas(pieces,joueur)
                    if Echectest[joueur-1][0]==True:
                        caseAsupprimer.append(k)
                    for niveau in pieces:
                        for sousniveau in niveau:
                            for piece in sousniveau:
                                if piece==piecebase:
                                    piece.position=ancpospiece
                    if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                        pieces=boitePieces.add_piece(pieceok2,pieces)
                    k += 1
                e=0
                for c in caseAsupprimer:
                    del caseCanMove[c-e]
                    e += 1
            elif piecebase.sousniveau==1:
                k=0
                caseAsupprimer=[]
                for case in caseCanMove:
                    boitePieces.pieces=pieces
                    if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                        for niveau in pieces:
                            for sousniveau in niveau:
                                for pieceok in sousniveau:
                                    if pieceok.position[0]==case[1] and pieceok.position[1]==case[0]:
                                        pieceok2=pieceok
                                        pieces=boitePieces.remove_piece(pieceok2,pieces)
                    for niveau in pieces:
                        for sousniveau in niveau:
                            for piece in sousniveau:
                                if piece==piecebase:
                                    piece.position=(case[1],case[0])
                    Echectest=echecOuPas(pieces,joueur)
                    if Echectest[joueur-1][0]==True:
                        caseAsupprimer.append(k)
                        ok += 1
                    for niveau in pieces:
                        for sousniveau in niveau:
                            for piece in sousniveau:
                                if piece==piecebase:
                                    piece.position=ancpospiece
                    if cases[case[1]-1][case[0]-1][0] == True and cases[case[1]-1][case[0]-1][1] != joueur:
                        pieces=boitePieces.add_piece(pieceok2,pieces)
                    k += 1
                e=0
                for c in caseAsupprimer:
                    del caseCanMove[c-e]
                    e += 1

    return caseCanMove

def checkMat(joueur,cases,boitePieces):
    """
    vérifie si il y a echec et mat
    """
    Grab=[[False,None],[False,None]]
    pieces=boitePieces.pieces
    bouger=False
    for niveau in pieces:
        for sousniveau in niveau:
            for piece in sousniveau:
                if bouger==False:
                    if piece.couleur==joueur:
                        if piece.niveau==1:
                            caseCanMove=casesPieceCanMove2(piece,pieces,joueur)
                            caseCanMove=changercasessiGrab2(False,piece,joueur,boitePieces,caseCanMove,cases,pieces,Grab)
                            if len(caseCanMove)>0:
                                bouger=True
                            for gardien in pieces[0][0]:
                                caseclic=gardien.position
                                casetest=[(caseclic[0],caseclic[1]+1),(caseclic[0]+1,caseclic[1]+1),(caseclic[0]+1,caseclic[1]),
                                (caseclic[0]+1,caseclic[1]-1),(caseclic[0],caseclic[1]-1),(caseclic[0]-1,caseclic[1]-1),(caseclic[0]-1,caseclic[1]),
                                (caseclic[0]-1,caseclic[1]+1)]
                                for case in casetest:
                                    if case==piece.position:
                                        Grab[joueur-1]=(True,piece)
                                        caseCanMove,ok=casesPieceCanMoveGrab2(piece,pieces,joueur,Grab)
                                        caseCanMove=changercasessiGrab2(ok,piece,joueur,boitePieces,caseCanMove,cases,pieces,Grab)
                        else:
                            caseCanMove=casesPieceCanMove2(piece,pieces,joueur)
                            caseCanMove=changercasessiGrab2(False,piece,joueur,boitePieces,caseCanMove,cases,pieces,Grab)
                        if len(caseCanMove)>0:
                            bouger=True
    if bouger==False:
        return joueur
    else:
        return 0