import pygame
import os
from .menu import Menu



images_fichier = os.path.join(os.path.dirname(__file__), 'images')
FichierAcceuil = os.path.join(images_fichier, '1.png')
FichierAcceuil2 = os.path.join(images_fichier, '2.png')
FichierAcceuil3 = os.path.join(images_fichier, '3.png')
FichierAcceuil4= os.path.join(images_fichier, '4.png')
FichierAcceuil5 = os.path.join(images_fichier, '5.png')
FichierAcceuil6 = os.path.join(images_fichier, '6.png')
FichierAcceuil7 = os.path.join(images_fichier, '7.png')
FichierAcceuil8 = os.path.join(images_fichier, '8.png')
FichierAcceuil9 = os.path.join(images_fichier, '9.png')
fichierJouer = os.path.join(images_fichier, '10.png')
fichierJouer2 = os.path.join(images_fichier, '11.png')
fichierJouer3 = os.path.join(images_fichier, '12.png')
fichierJouer4 = os.path.join(images_fichier, '13.png')
fichierJouer5 = os.path.join(images_fichier, '14.png')
fichierJouer6 = os.path.join(images_fichier, '15.png')
fichierJouer7 = os.path.join(images_fichier, '16.png')
fichierJouer8 = os.path.join(images_fichier, '17.png')
fichierCompte = os.path.join(images_fichier, '18.png')
fichierCompte2 = os.path.join(images_fichier, '19.png')
fichierCompte3 = os.path.join(images_fichier, '20.png')
fichierCompte4 = os.path.join(images_fichier, '21.png')
fichierCompte5 = os.path.join(images_fichier, '22.png')
fichierCompte6 = os.path.join(images_fichier, '23.png')
fichierCompte7 = os.path.join(images_fichier, '24.png')
fichierClassement = os.path.join(images_fichier, '25.png')
fichierClassement2 = os.path.join(images_fichier, '26.png')
fichierClassement3 = os.path.join(images_fichier, '27.png')
fichierClassement4 = os.path.join(images_fichier, '28.png')
fichierClassement5 = os.path.join(images_fichier, '29.png')
fichierClassement6 = os.path.join(images_fichier, '30.png')
fichierClassement7 = os.path.join(images_fichier, '31.png')
fichierRegles = os.path.join(images_fichier, '32.png')
fichierRegles2 = os.path.join(images_fichier, '33.png')
fichierRegles3 = os.path.join(images_fichier, '34.png')
fichierRegles4 = os.path.join(images_fichier, '35.png')
fichierRegles5 = os.path.join(images_fichier, '36.png')
fichierRegles6 = os.path.join(images_fichier, '37.png')
fichierRegles7 = os.path.join(images_fichier, '38.png')
fichierParametres = os.path.join(images_fichier, '39.png')
fichierParametres2 = os.path.join(images_fichier, '40.png')
fichierParametres3 = os.path.join(images_fichier, '41.png')
fichierParametres4 = os.path.join(images_fichier, '42.png')
fichierParametres5 = os.path.join(images_fichier, '43.png')
fichierParametres6 = os.path.join(images_fichier, '44.png')
fichierParametres7 = os.path.join(images_fichier, '45.png')
fichierConnexion = os.path.join(images_fichier, '46.png')
fichierConnexion2 = os.path.join(images_fichier, '47.png')
fichierConnexion3 = os.path.join(images_fichier, '48.png')
fichierConnexion4 = os.path.join(images_fichier, '49.png')
fichierConnexion5 = os.path.join(images_fichier, '50.png')
fichierConnexion6 = os.path.join(images_fichier, '51.png')
fichierConnexion7 = os.path.join(images_fichier, '52.png')
fichierConnexion8 = os.path.join(images_fichier, '53.png')
fichierConnexion9 = os.path.join(images_fichier, '54.png')
fichierConnexion10 = os.path.join(images_fichier, '55.png')
fichierConnexion11 = os.path.join(images_fichier, '56.png')
fichierConnexion12 = os.path.join(images_fichier, '57.png')
fichierConnexion13 = os.path.join(images_fichier, '58.png')
fichierConnexion14 = os.path.join(images_fichier, '59.png')
fichierConnexion15 = os.path.join(images_fichier, '60.png')
fichierFin = os.path.join(images_fichier, '63.png')
fichierFin2 = os.path.join(images_fichier, '64.png')
fichierFin3 = os.path.join(images_fichier, '65.png')
fichierFin4 = os.path.join(images_fichier, '66.png')
fichierFin5 = os.path.join(images_fichier, '67.png')
fichierFin6 = os.path.join(images_fichier, '68.png')
fichierFin7 = os.path.join(images_fichier, '69.png')



fichierGif = os.path.join(images_fichier,"Gif1")

def loadFrames(folder_path):
    """Charge toutes les frames d'un gif

    Args:
        folder_path (str): chemin du fichier

    Returns:
        frames : liste de toute les images du gif
    """
    frames = []
    for filename in os.listdir(folder_path):
        if filename.endswith(".png"):
                img = pygame.image.load(os.path.join(folder_path, filename))
                frames.append(img)  
    return frames

def chargerMenu(fenetre):
    """Charge tout les menus

    Args:
        fenetre (object): fenetre pygame

    Returns:
        menus : tuple de tout les menus
    """
    element = [(pygame.image.load(FichierAcceuil),(0,0)),(pygame.image.load(FichierAcceuil2),(0,0)),(pygame.image.load(FichierAcceuil3),(0,0)),(pygame.image.load(FichierAcceuil4),(0,0)),
            (pygame.image.load(FichierAcceuil5),(0,0)),(pygame.image.load(FichierAcceuil6),(0,0)),(pygame.image.load(FichierAcceuil7),(0,0)),(pygame.image.load(FichierAcceuil8),(0,0)),
            (pygame.image.load(FichierAcceuil9),(0,0))]
    elementClic = [(0,10,350,100),(0,150,350,95),(0,255,350,95),(0,360,350,95),(0,465,350,95),(0,888,350,95),(0,986,350,95),(1233,111,642,169),(1233,353,642,169)]
    menu = Menu(fenetre,element,[],elementClic)

    elementJouer = [(pygame.image.load(fichierJouer),(0,0)),(pygame.image.load(fichierJouer2),(0,0)),(pygame.image.load(fichierJouer3),(0,0)),(pygame.image.load(fichierJouer4),(0,0)),
                    (pygame.image.load(fichierJouer5),(0,0)),(pygame.image.load(fichierJouer6),(0,0)),(pygame.image.load(fichierJouer7),(0,0)),(pygame.image.load(fichierJouer8),(0,0))]
    elementClicJouer = [(0,10,350,100),(0,150,350,95),(0,255,350,95),(0,360,350,95),(0,465,350,95),(0,888,350,95),(0,986,350,95),(1442,425,360,169)]
    menuJouer = Menu(fenetre,elementJouer,[],elementClicJouer)

    elementCompte = [(pygame.image.load(fichierCompte),(0,0)),(pygame.image.load(fichierCompte2),(0,0)),(pygame.image.load(fichierCompte3),(0,0)),(pygame.image.load(fichierCompte4),(0,0)),
                    (pygame.image.load(fichierCompte5),(0,0)),(pygame.image.load(fichierCompte6),(0,0)),(pygame.image.load(fichierCompte7),(0,0))]
    elementClicCompte = [(0,10,350,100),(0,150,350,95),(0,255,350,95),(0,360,350,95),(0,465,350,95),(0,888,350,95),(0,986,350,95)]
    menuCompte = Menu(fenetre,elementCompte,[],elementClicCompte)

    elementClassement = [(pygame.image.load(fichierClassement),(0,0)),(pygame.image.load(fichierClassement2),(0,0)),(pygame.image.load(fichierClassement3),(0,0)),(pygame.image.load(fichierClassement4),(0,0)),
                        (pygame.image.load(fichierClassement5),(0,0)),(pygame.image.load(fichierClassement6),(0,0)),(pygame.image.load(fichierClassement7),(0,0))]
    elementClicClassement = [(0,10,350,100),(0,150,350,95),(0,255,350,95),(0,360,350,95),(0,465,350,95),(0,888,350,95),(0,986,350,95)]
    menuClassement = Menu(fenetre,elementClassement,[],elementClicClassement)

    elementRegles = [(pygame.image.load(fichierRegles),(0,0)),(pygame.image.load(fichierRegles2),(0,0)),(pygame.image.load(fichierRegles3),(0,0)),(pygame.image.load(fichierRegles4),(0,0)),
                    (pygame.image.load(fichierRegles5),(0,0)),(pygame.image.load(fichierRegles6),(0,0)),(pygame.image.load(fichierRegles7),(0,0))]
    elementClicParametre = [(0,10,350,100),(0,150,350,95),(0,255,350,95),(0,360,350,95),(0,465,350,95),(0,888,350,95),(0,986,350,95)]
    menuRegle = Menu(fenetre,elementRegles,[],elementClicParametre)

    elementParametre = [(pygame.image.load(fichierParametres),(0,0)),(pygame.image.load(fichierParametres2),(0,0)),(pygame.image.load(fichierParametres3),(0,0)),(pygame.image.load(fichierParametres4),(0,0)),
                        (pygame.image.load(fichierParametres5),(0,0)),(pygame.image.load(fichierParametres6),(0,0)),(pygame.image.load(fichierParametres7),(0,0))]
    elementClicParametre = [(0,10,350,100),(0,150,350,95),(0,255,350,95),(0,360,350,95),(0,465,350,95),(0,888,350,95),(0,986,350,95)]
    menuParametre = Menu(fenetre,elementParametre,[],elementClicParametre)

    elementConnexion = [(pygame.image.load(fichierConnexion),(0,0)),(pygame.image.load(fichierConnexion2),(0,0)),(pygame.image.load(fichierConnexion3),(0,0))]
    elementClicConnexion = [(0,0,0,0),(664,540,590,77),(664,674,590,77)]
    menuConnexion = Menu(fenetre,elementConnexion,[],elementClicConnexion)

    elementConnexion2 = [(pygame.image.load(fichierConnexion4),(0,0)),(pygame.image.load(fichierConnexion5),(0,0)),(pygame.image.load(fichierConnexion6),(0,0))]
    elementClicConnexion2 = [(1255,754,50,50),(664,957,590,77),(664,854,590,77)]
    menuConnexion2 = Menu(fenetre,elementConnexion2,[],elementClicConnexion2)

    elementConnexion3 = [(pygame.image.load(fichierConnexion7),(0,0)),(pygame.image.load(fichierConnexion8),(0,0)),(pygame.image.load(fichierConnexion9),(0,0))]
    elementClicConnexion3 = [(1255,754,50,50),(664,957,590,77),(664,854,590,77)]
    menuConnexion3 = Menu(fenetre,elementConnexion3,[],elementClicConnexion3)

    elementConnexion4 = [(pygame.image.load(fichierConnexion10),(0,0)),(pygame.image.load(fichierConnexion11),(0,0)),(pygame.image.load(fichierConnexion12),(0,0))]
    elementClicConnexion4 = [(1255,601,50,50),(664,849,590,77),(664,749,590,77)]
    menuConnexion4 = Menu(fenetre,elementConnexion4,[],elementClicConnexion4)

    elementConnexion5 = [(pygame.image.load(fichierConnexion13),(0,0)),(pygame.image.load(fichierConnexion14),(0,0)),(pygame.image.load(fichierConnexion15),(0,0))]
    elementClicConnexion5 = [(1255,601,50,50),(664,849,590,77),(664,749,590,77),]
    menuConnexion5 = Menu(fenetre,elementConnexion5,[],elementClicConnexion5)

    elementAttente = [loadFrames(fichierGif)]
    menuAttente = Menu(fenetre,elementAttente,[],[])

    elementFin = [(pygame.image.load(fichierFin),(0,0)),(pygame.image.load(fichierFin2),(0,0)),(pygame.image.load(fichierFin3),(0,0)),(pygame.image.load(fichierFin4),(0,0)),
                  (pygame.image.load(fichierFin5),(0,0)),(pygame.image.load(fichierFin6),(0,0)),(pygame.image.load(fichierFin7),(0,0))]
    menuFin= Menu(fenetre,elementFin,[(0,0,1920,1080)],[])
    menus = (menu, menuJouer,menuCompte,menuClassement,menuRegle,menuParametre,menuConnexion,menuConnexion2,menuConnexion3,menuConnexion4,menuConnexion5,menuAttente,menuFin)
    return menus
