import pygame


class Menu():
    def __init__(self,fenetre:object,element:list,texts:list,elementClic:list):
        self.fenetre = fenetre
        self.element = element
        self.texts = texts
        self.elementClic = elementClic

    
    def afficherMenu(self,i):
        """affiche le menu à l'indice i

        Args:
            i (int): indice du menu
        """
        self.fenetre.blit(self.element[i][0],self.element[i][1])
        
    
    def clic(self,x,y):
        """regarde si il y a un clic sur un élément

        Args:
            x (int): position x de la souris
            y (int): position y de la souris

        Returns:
            liste  : contient les informations des éléments cliqués ou non
        """
        returnElement = []
        for i in range(len(self.elementClic)):
            element = pygame.Rect(self.elementClic[i][0],self.elementClic[i][1],self.elementClic[i][2],self.elementClic[i][3])
            if element.collidepoint(x,y):
                returnElement.append(True)
            else:
                returnElement.append(False)
        return returnElement
    
    def hover(self,x,y):
        """regarde si on a la souris sur un élément

        Args:
            x (int): position x de la souris
            y (int): position y de la souris

        Returns:
            renvoie l'indice de l'element sur lequel il y a la souris
        """
        for i in range(len(self.elementClic)):
            element = pygame.Rect(self.elementClic[i][0],self.elementClic[i][1],self.elementClic[i][2],self.elementClic[i][3])
            if element.collidepoint(x,y):
                return i
        return 0     

                    
                
                        
