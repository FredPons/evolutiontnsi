import time
import socket
from receiveData import socket_receive_all_data

class Partie:
    def __init__(self, joueur1, joueur2,order):
        self.joueur1 = joueur1
        self.joueur2 = joueur2
        self.order = order

    def jouer(self,server_socket):
        """Boucle principale d'une partie pour envoyé les données d'un joueur à un autre

        Args:
            server_socket (_type_): _description_

        Returns:
            tuple : Fin de partie et le gagnant
        """
        print('odre des joueurs')
        data = "p1".encode()
        data_len = len(data)
        header = str(data_len).zfill(13)
        print("header:", header)
        self.joueur1.client_socket.sendall(header.encode())
        time.sleep(0.2)
        if data_len > 0:
            self.joueur1.client_socket.sendall(data)
        time.sleep(0.2)
        print("joueur 1 définie")
        time.sleep(0.2)
        data = "p2".encode()
        data_len = len(data)
        header = str(data_len).zfill(13)
        print("header:", header)
        self.joueur2.client_socket.sendall(header.encode())
        time.sleep(0.2)
        if data_len > 0:
            self.joueur2.client_socket.sendall(data)
        print("joueur 2 définie")
        time.sleep(0.2)
        print('debut')
        data = "play".encode()
        data_len = len(data)
        header = str(data_len).zfill(13)
        print("header:", header)
        self.joueur1.client_socket.sendall(header.encode())
        time.sleep(0.2)
        if data_len > 0:
            self.joueur1.client_socket.sendall(data)
        time.sleep(0.2)
        data = "wait".encode()
        data_len = len(data)
        header = str(data_len).zfill(13)
        print("header:", header)
        self.joueur2.client_socket.sendall(header.encode())
        time.sleep(0.2)
        if data_len > 0:
            self.joueur2.client_socket.sendall(data)
        time.sleep(0.2)
        while True:
            # Le joueur 1 envoie un message au serveur
            try:
                header_data = socket_receive_all_data(server_socket,self.joueur1.client_socket, 13)
                longeur_data = int(header_data.decode())
                print(longeur_data)

                data = socket_receive_all_data(server_socket,self.joueur1.client_socket, longeur_data)
                if not data:
                    # Le serveur a fermé la connexion
                    print("Connexion au serveur perdue.")
                
            except socket.error:
                self.joueur1.is_connected = False
                # Gérer les erreurs de connexion
                print("Erreur de socket.")
                t0 = time.time() + 30 
                while time.time() < t0 and not self.joueur1.is_connected:
                    reponse = f"DISC,{t0-time.time()}".encode()
                    data_len = len(reponse)
                    header = str(data_len).zfill(13)
                    print("header:", header)
                    self.joueur2.client_socket.sendall(header.encode())
                    time.sleep(0.2)
                    if data_len > 0:
                        self.joueur2.client_socket.sendall(reponse)
                    time.sleep(0.2)
                if not self.joueur1.is_connected:
                    self.joueur2.client_socket.sendall("END,6".encode())
            # Le serveur envoie le message au joueur 2
            
            try:
                data_len = len(data)
                header = str(data_len).zfill(13)
                print("header:", header)
                self.joueur2.client_socket.sendall(header.encode())
                time.sleep(0.2)
                if data_len > 0:
                    self.joueur2.client_socket.sendall(data)
                time.sleep(0.2)
            except socket.error:
                self.joueur1.is_connected = False
                # Gérer les erreurs de connexion
                print("Erreur de socket.")
                t0 = time.time() + 30 
                while time.time() < t0 and not self.joueur1.is_connected:
                    reponse = f"DISC,{t0-time.time()}".encode()
                    data_len = len(reponse)
                    header = str(data_len).zfill(13)
                    print("header:", header)
                    self.joueur2.client_socket.sendall(header.encode())
                    time.sleep(0.2)
                    if data_len > 0:
                        self.joueur2.client_socket.sendall(reponse)
                    time.sleep(0.2)
                if not self.joueur1.is_connected:
                    self.joueur2.client_socket.sendall("END,6".encode())
            data = data.decode()
            if data == "END,1":
               return (self.joueur2,"W"),(self.joueur1,"L")     
            elif data == "END,2":
                return (self.joueur2,"W"),(self.joueur1,"L")    
            elif data == "END,3":
                return (self.joueur2,"D"),(self.joueur1,"D")    
            elif data == "END,4":
                return (self.joueur2,"W"),(self.joueur1,"L")   
            elif data == "END,5":
                return (self.joueur2,"D"),(self.joueur1,"D")     
            # Le joueur 2 envoie un message au serveur
            try:
                header_data = socket_receive_all_data(server_socket,self.joueur2.client_socket, 13)
                longeur_data = int(header_data.decode())
                print(longeur_data)

                data = socket_receive_all_data(server_socket,self.joueur2.client_socket, longeur_data)
                print("Joueur 2 : " )
                print(data)
                if not data:
                    # Le serveur a fermé la connexion
                    print("Connexion au serveur perdue.")
                    
                    
            except socket.error:
                self.joueur2.is_connected = False
                # Gérer les erreurs de connexion
                print("Erreur de socket.")
                t0 = time.time() + 30 
                while time.time() < t0 and not self.joueur2.is_connected:
                    reponse = f"DISC,{t0-time.time()}".encode()
                    data_len = len(reponse)
                    header = str(data_len).zfill(13)
                    print("header:", header)
                    self.joueur1.client_socket.sendall(header.encode())
                    time.sleep(0.2)
                    if data_len > 0:
                        self.joueur1.client_socket.sendall(reponse)
                    time.sleep(0.2)
                if not self.joueur2.is_connected:
                    self.joueur1.client_socket.sendall("END,6".encode())
            # Le serveur envoie le message au joueur 1
            
            try:
                data_len = len(data)
                header = str(data_len).zfill(13)
                print("header:", header)
                self.joueur1.client_socket.sendall(header.encode())
                time.sleep(0.2)
                if data_len > 0:
                    self.joueur1.client_socket.sendall(data)
                time.sleep(0.2)
                    
            except socket.error:
                self.joueur2.is_connected = False
                # Gérer les erreurs de connexion
                print("Erreur de socket.")
                t0 = time.time() + 30 
                while time.time() < t0 and not self.joueur2.is_connected:
                    reponse = f"DISC,{t0-time.time()}".encode()
                    data_len = len(reponse)
                    header = str(data_len).zfill(13)
                    print("header:", header)
                    self.joueur1.client_socket.sendall(header.encode())
                    time.sleep(0.2)
                    if data_len > 0:
                        self.joueur1.client_socket.sendall(reponse)
                    time.sleep(0.2)
                if not self.joueur2.is_connected:
                    self.joueur1.client_socket.sendall("END,6".encode())
            data = data.decode()
            if data == "END,1":
               return (self.joueur1,"W"),(self.joueur2,"L")     
            elif data == "END,2":
                return (self.joueur1,"W"),(self.joueur2,"L")    
            elif data == "END,3":
                return (self.joueur1,"D"),(self.joueur2,"D")    
            elif data == "END,4":
                return (self.joueur1,"W"),(self.joueur2,"L")   
            elif data == "END,5":
                return (self.joueur1,"D"),(self.joueur2,"D") 