def tri(liste):
    """Tri la liste en fonction de l'elo

    Args:
        liste (list): liste des joueurs

    Returns:
        liste (list) : liste des joueurs trié
    """
    if len(liste) <= 1:
        return liste
    else:
        pivot = liste[0]
        plus_petits = [x for x in liste[1:] if x.elo < pivot.elo]
        plus_grands = [x for x in liste[1:] if x.elo >= pivot.elo]
        return tri(plus_petits) + [pivot] + tri(plus_grands)

def matchmaking(q1,q2):
    """Fait le matchmaking en fonction de l'elo

    Args:
        q1 (object): queue pour transferer les données
        q2 (object): queue pour transferer les données
    """
    queueJoueur = []
    while True:
        while len(queueJoueur) < 2:
            try:
                newJ = q1.get_nowait()
                print(f"newJ = {newJ}")
                print(f"joueur recu : {newJ.pseudo}")
                queueJoueur.append(newJ)
            except:
                continue
        newparties=[]
        queueJoueur=tri(queueJoueur)
        if len(queueJoueur)%2==0:
            for joueur in queueJoueur:
                if queueJoueur.index(joueur)%2==0:
                    newparties.append((joueur,queueJoueur[queueJoueur.index(joueur)+1]))
                    queueJoueur.pop(queueJoueur.index(joueur)+1)
                    queueJoueur.pop(queueJoueur.index(joueur))
        else:
            pWait = queueJoueur[-1]
            queueJoueur.pop(-1)
            for joueur in queueJoueur:
                if queueJoueur.index(joueur)%2==0:
                    newparties.append((joueur,queueJoueur[queueJoueur.index(joueur)+1]))
            queueJoueur.append(pWait)
        if len(newparties) >= 1:
            for i in range(len(newparties)*2):
                q2.put(newparties)