def socket_receive_all_data(socket,client_socket, data_len):
    """Récupère les données en fonction d'un nombre d'octets définit par data_len

    Args:
        socket (object): socket du joueur
        data_len (int): nombre d'octets

    Returns:
        total_data : data récuperer via le socket
    """
    MAX_DATA_SIZE = 1024
    current_data_len = 0
    total_data = b""
    while current_data_len < data_len:
        chunk_len = min(data_len - current_data_len, MAX_DATA_SIZE)
        try : 
            data = client_socket.recv(chunk_len)
            if not data:
                raise socket.error("Connexion interrompue")
        except socket.error:
            raise Exception("Erreur réception données")
        total_data += data
        current_data_len += len(data)
    print(total_data)
    return total_data