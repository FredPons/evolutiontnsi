import threading
import random
from bdd import addUser, getUserByEmail, getUserByName, updateUserStats
from partie import Partie

class GestionClient(threading.Thread):

    def __init__(self, client_socket, client_address,q1,q2,server_socket):
        super().__init__()
        self.client_socket = client_socket
        self.client_address = client_address
        self.client_id = None
        self.pseudo = None
        self.elo = None
        self.gamePlayed = None
        self.gameWon = None
        self.gameLose = None
        self.gameDrawn = None
        self.history = None
        self.is_connected = None
        self.q1 = q1
        self.q2 = q2
        self.server_socket = server_socket

    def run(self):
        """Boucle principale de la gestion des clients
        """
        client_id,pseudo,email,elo,gamePlayed,gameWon,gameLose,gameDrawn,history = self.connectionMethod() 
        self.client_id = client_id
        self.pseudo = pseudo
        self.email =email
        self.elo = elo
        self.gamePlayed = gamePlayed
        self.gameWon = gameWon
        self.gameLose = gameLose
        self.gameDrawn = gameDrawn
        self.history =history
        self.is_connected = True
        print(f"Client {self.client_id} connecté depuis {self.client_address}")
        try:
            data = self.client_socket.recv(1024).decode()
            if not data:
                print(f"Client {self.client_id} déconnecté.")
                return
            if data == "start":
                self.q1.put(self)  
                newPartie=self.q2.get()
                for partie in newPartie:
                    if partie[0] == self:
                        order = random.randint(0,1)
                        game= Partie(partie[0],partie[1],order)
                        newPartie.remove(partie)
                        resultat = game.jouer(self.server_socket)
                        if resultat[0][1] =="W":
                            updateUserStats(userName=resultat[0][0].pseudo,elo= resultat[0][0].elo+10,gamesPlayed=resultat[0][0].gamePlayed+1,gamesWon=resultat[0][0].gameWon+1,history=resultat[0][0].history+f",({resultat[1][0].pseudo} : Gagné )")
                            updateUserStats(userName=resultat[1][0].pseudo,elo=resultat[1][0].elo-10,gamesPlayed=resultat[1][0].gamePlayed+1,gamesLost=resultat[1][0].gameLose+1,history=resultat[1][0].history+f",({resultat[0][0].pseudo} : Perdu )")
                        elif resultat[0][1] =="D":
                            updateUserStats(userName=resultat[0][0].pseudo,gamesPlayed=resultat[0][0].gamePlayed+1,gamesDrawn=resultat[0][0].gameDrawn+1,history=resultat[0][0].history+f",({resultat[1][0].pseudo} : Nul )")
                            updateUserStats(userName=resultat[1][0].pseudo,gamesPlayed=resultat[1][0].gamePlayed+1,gamesDrawn=resultat[1][0].gameDrawn+1,history=resultat[1][0].history+f",({resultat[0][0].pseudo} : Nul )")
                    elif partie[1] == self:
                        pass

        except ConnectionResetError:
            
            print(f"Connexion au client {self.client_id} perdue.")
            self.client_socket.close()
            return

    def connectionMethod(self):
        """Permet à l'utilisateur de se connecter à la base de données

        Returns:
            userInfo : liste des informations du joueur
        """
        while True:
            try:
                data = self.client_socket.recv(1024).decode()
                if data == "login":
                    while True:
                        try:
                            data2 = self.client_socket.recv(1024).decode()
                            if data == "exit":
                                break
                            data2 = data2.split(",")
                            if len(data2[0]) > 14 :
                                userInfo = getUserByEmail(data2[0])
                                if userInfo is None:
                                    self.client_socket.sendall("Cet email n'existe pas".encode())
                                elif userInfo[3] != data2[1]:
                                    self.client_socket.sendall("Mot de passe incorrect".encode())
                                else:
                                    self.client_socket.sendall("Success".encode())
                                    return  userInfo[0],userInfo[1],userInfo[2],userInfo[4],userInfo[5],userInfo[6],userInfo[7],userInfo[8],userInfo[9]
                            else: 
                                userInfo = getUserByName(data2[0])
                                if userInfo is None:
                                    self.client_socket.sendall("Ce pseudo n'existe pas".encode())
                                elif userInfo[3] != data2[1]:
                                    self.client_socket.sendall("Mot de passe incorrect".encode())
                                else:
                                    self.client_socket.sendall("Success".encode())
                                    return  userInfo[0],userInfo[1],userInfo[2],userInfo[4],userInfo[5],userInfo[6],userInfo[7],userInfo[8],userInfo[9]
                        except  Exception as e:
                            print(str(e))
                            break    
                elif data == "signup":
                    while True:
                        try:
                            data2 = self.client_socket.recv(1024).decode()
                            if data2 == "exit":
                                break
                            data2 = data2.split(",")
                            SuccessOrError = addUser(data2[0],data2[1],data2[2])
                            if SuccessOrError == "Success":
                                userInfo = getUserByName(data2[0]) 
                                self.client_socket.sendall("Success".encode())
                                return  userInfo[0],userInfo[1],userInfo[2],userInfo[4],userInfo[5],userInfo[6],userInfo[7],userInfo[8],userInfo[9]
                            else:
                                self.client_socket.send(SuccessOrError.encode())
                        except  Exception as e:
                            print(str(e))
                            break
            except Exception as e:
                print(f"Le client {self.client_id} s'est deconnecter :  {str(e)}")


    
    def __repr__(self) -> str:
        return f"Client n°{self.client_id} connécter à l'adresse : {self.client_address} en tant que {self.pseudo}"