import socket
import threading
import queue
from matchmaking import matchmaking
from gestionclient import GestionClient
from bdd import createDatabase


# Définition de la fonction principale pour gérer les connexions entrantes
def main():    
    """Gère les connections entrante et crée un thread pour chaques joueurs
    """


    q1 = queue.Queue()
    q2 = queue.Queue()
    # Configuration du serveur
    #host = '2a02:8428:1b52:2601:cfb2:33a:d5:10b0'
    host= "192.168.1.6"
    #host = "localhost"
    port = 5555
    # Création du socket
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


    # Liaison du socket au port et démarrage de l'écoute
    server_socket.bind((host, port))
    server_socket.listen()

    print(f"Serveur démarré sur le port {port}")
    
   
    thread_matchmaking = threading.Thread(target=matchmaking, args=(q1,q2))
    thread_matchmaking.start()
    # Boucle principale pour gérer les connexions entrantes
    while True:
        # Attente d'une connexion
        client_socket, client_address = server_socket.accept()

        # Lancement d'un thread pour gérer la connexion
        gestionClient = GestionClient(client_socket, client_address,q1,q2,server_socket)
        gestionClient.start()

if __name__ == '__main__':
    createDatabase()
    main()
    






    