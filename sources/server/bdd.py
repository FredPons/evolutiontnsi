import sqlite3


def createDatabase():
    """Crée ou se connect à la base de données
    """
    conn = sqlite3.connect('users.db')  
    cursor = conn.cursor()  

    cursor.execute('''
    CREATE TABLE IF NOT EXISTS users (
        id INTEGER PRIMARY KEY,
        pseudo TEXT(14) UNIQUE NOT NULL,
        email TEXT UNIQUE NOT NULL,
        password TEXT NOT NULL,
        elo INTEGER DEFAULT 1000,
        games_played INTEGER DEFAULT 0,
        games_won INTEGER DEFAULT 0,
        games_lost INTEGER DEFAULT 0,
        games_drawn INTEGER DEFAULT 0,
        history TEXT DEFAULT ''
    );
    ''')

    conn.commit()  
    conn.close()  

def addUser(pseudo, email, password):
    """Ajoute un joueur à la base de données

    Args:
        pseudo (str): pseudo du joueur
        email (str): email du joueur
        password (str): mot de passe du joueur

    Returns:
        Success si c'est réussi , sinon une erreur
    """
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()

    try:
        cursor.execute('''
        INSERT INTO users (pseudo, email, password)
        VALUES (?, ?, ?)
        ''', (pseudo, email, password))
        conn.commit()
        conn.close()
        return "Success"
    except sqlite3.IntegrityError:
        error = "L'email et/ou le pseudo sont déjà utilisés ."
        conn.close()
        return error

def getUserByName(userName):
    """Recupère les information en fonction du pseudo

    Args:
        userName (str): pseudo du joueur

    Returns:
        user : information de du joueur
    """
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM users WHERE pseudo = ?', (userName,))
    user = cursor.fetchone()

    conn.close()

    return user

def getUserByEmail(userEmail):
    """Recupère les information en fonction de l'email
    Args:
        userName (str): pseudo du joueur

    Returns:
        user : information de du joueur
    """
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM users WHERE email = ?', (userEmail,))
    user = cursor.fetchone()

    conn.close()

    return user

def updateUserStats(userName, elo=None, gamesPlayed=None, gamesWon=None, gamesLost=None, gamesDrawn=None ,history = None):
    """Modifie les  informations d'un utilisateur

    Args:
        userName (_type_): _description_
        elo (_type_, optional): _description_. Defaults to None.
        gamesPlayed (_type_, optional): _description_. Defaults to None.
        gamesWon (_type_, optional): _description_. Defaults to None.
        gamesLost (_type_, optional): _description_. Defaults to None.
        gamesDrawn (_type_, optional): _description_. Defaults to None.
        history (_type_, optional): _description_. Defaults to None.
    """
    conn = sqlite3.connect('users.db')
    cursor = conn.cursor()

    updateQuery = 'UPDATE users SET '
    updateParams = []

    if elo is not None:
        updateQuery += 'elo = ?, '
        updateParams.append(elo)

    if gamesPlayed is not None:
        updateQuery += 'games_played = ?, '
        updateParams.append(gamesPlayed)

    if gamesWon is not None:
        updateQuery += 'games_won = ?, '
        updateParams.append(gamesWon)

    if gamesLost is not None:
        updateQuery += 'games_lost = ?, '
        updateParams.append(gamesLost)

    if gamesDrawn is not None:
        updateQuery += 'games_drawn = ?, '
        updateParams.append(gamesDrawn)
    
    if history is not None:
        updateQuery += 'history = ?, '
        updateParams.append(history)

    updateQuery = updateQuery.rstrip(', ')

    updateQuery += ' WHERE pseudo = ?'
    updateParams.append(userName)

    cursor.execute(updateQuery, tuple(updateParams))
    conn.commit()

    conn.close()





    
            
    