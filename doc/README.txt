

Après avoir installer tout les modules  nécessaires, vous pouvez commencer à jouer.

Lancer dans un premier temps le server.py en vérifiant que l'host est "localhost" et le port celui de votre choix .

Ensuite lancer une première fois le main.py et crée vous un compte.
Faite à nouveau cette étape pour le deuxième joueur . 

A savoir pour jouer sur ordinateur différent depuis un réseau différent il faut redirigé le port dans les paramètres de la box .
Je vous conseille donc de tester en local avec deux clients différents sur une même machine.

Pour lancer une partie appuyer sur le bouton jouer pour les deux joueurs. 

**Un bug peut survenir apres la fin d'une partie si vous rejouer sans relancer le programme .

Pour la version web avec le framework django : 

    Créer un environnement virtuel avec la commande python -m venv .env     (.env -> nom de l'env)
    Activer l'env  -> .env\Scripts\activate.bat                  (attention à bien être dans la racine du projet)
    installer les modules  necessaire dans l'environement virtuel (pip install -r requirements.txt)
    Faire cd Evolution pour se placer dans le dossier où se trouve le fichier manage.py
    Lancer le serveur avec la commande python manage.py runserver dans le terminal 
    Aller sur un navigateur et taper 127.0.0.1:8000 
    Créer vous un compte 
    Faites la meme chose en navigation privé pour ne pas se reconnecter et pouvoir lancer une partie .


    **La gestion de partie n'est pas finis**

    Pour acceder à la base de données il faut se connecter en administrateur en tapant  127.0.0.1:8000/admin/
    nom d'utilisateur : admin
    mot de passe : evolutionadmin

